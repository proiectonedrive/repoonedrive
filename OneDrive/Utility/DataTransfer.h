#pragma once
#include <stdlib.h>
#include <winsock2.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <exception>
#include "FileHandler.h"
#include "ServerClientCommunicationCodes.h"
#include <charconv>
namespace DataTransfer
{
#define maximum_size_digit_count 20

	//Basic transfer functions **************************************
	
	// recieves data
	int64_t RecvBuffer(SOCKET sock, char* buffer, const uint64_t& buffer_size,
		const uint32_t& chunk_size = 4 * 1024);

	//recieves text
	std::string RecvText(SOCKET sock);

	//sends data
	int64_t SendBuffer(SOCKET sock, const char* buffer, const uint64_t& buffer_size,
		const uint32_t& chunk_size = 4 * 1024);

	//sends text
	int64_t SendText(SOCKET sock, const std::string& text);


	// File transfer functions *****************************************
	
	//recieves file
	int64_t RecvFile(SOCKET sock, const std::string& working_dir_path, const uint64_t& chunk_size = 64 * 1024);

	//recieve a directory
	int64_t RecvDir(SOCKET sock, const std::string& working_dir_path);

	//standardizes sizes with the format of 20 digit string
	std::string StandardizeSizeString(const uint64_t& size);

	//determines the size of the file that will be sent
	uint64_t GetFileSize(const std::string& file_name);

	//sends specified file
	int64_t SendFile(SOCKET sock, const std::string& file_name, const std::string& working_dir_path, const uint32_t& chunk_size = 64 * 1024);

	//sends a directory and all it's files and subdirectories
	int64_t SendDir(SOCKET sock, const std::string& dir_path);


	// Communication codes transfer functions *****************************************

	bool SendLoginCode(SOCKET sock, const CommunicationCodes::LoginCodes& code);
	bool SendConnectionHandlerCode(SOCKET sock, const CommunicationCodes::ConnectionHandlerCodes& code);
	bool SendFileTypeCode(SOCKET sock, const CommunicationCodes::FileTypeCodes& code);
	bool SendAnswersCode(SOCKET sock, const CommunicationCodes::AnswersCodes& code);

	CommunicationCodes::LoginCodes RecvLoginCode(SOCKET sock);
	CommunicationCodes::ConnectionHandlerCodes RecvConnectionHandlerCode(SOCKET sock);
	CommunicationCodes::FileTypeCodes RecvFileTypeCode(SOCKET sock);
	CommunicationCodes::AnswersCodes RecvAnswersCode(SOCKET sock);
}