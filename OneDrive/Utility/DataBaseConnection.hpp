#pragma once
#include <iostream>
#include <unordered_map>
#include <sqlite3.h>
#include <string>


namespace DataBaseConnection {

    // This function creates the connection to the database
    extern int createDB(const char* db_path);

    // This function creates a table in the database
    extern int createTable(const char* db_path, std::string sql);

    // This function inserts data into the table
    extern bool insertData(const char* db_path, std::string sql);

    // This function updates the data in a table
    extern int updateData(const char* db_path, std::string sql);

    // This function selects data from the table
    extern int selectData(const char* db_path, std::string sql);

    // Retrieve contents of database used by selectData()
    extern int callback(void* NotUsed, int argc, char** argv, char** azColName);

    // This function deletes data from the table
    extern int deleteData(const char* db_path, std::string sql);

    // This function selects path and hash data from the table (file)and places them in an unorderd_map (file_map)
    extern int selectFile(const char* db_path, std::unordered_map < std::string, std::string >& file_map, int id_user);

    // This function searches the database for a specific username and returns a bool accordingly
    extern bool findUser(const char* db_path, const std::string& username);

    // This function searches the database for a specific device name and returns a bool accordingly
    extern bool findDevice(const char* db_path, const std::string& username, const std::string& device_name);

    // This function searches the database for a specific username and returns its id
    extern int getUserId(const char* db_path, const std::string& username);

    extern std::string getFileHash(const char* db_path, const std::string& path, int id_user);
}

