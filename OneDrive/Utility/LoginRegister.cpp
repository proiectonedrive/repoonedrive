#include "LoginRegister.hpp"
#include <regex>
// Function that checks if the given username respects the validation rules
bool lr::ValidateUsername(const std::string& username)
{
	if (std::regex_match(username, std::regex("(?=.{5,30}$)([a-zA-Z]([a-zA-Z0-9])*([.]|_)?([a-zA-Z0-9])*)")))
		return true;

	return false;
}
