#pragma once
#include <filesystem>
#include <string>

namespace fh
{	
	// Copy functions *****************************************
	
	//copies a single file to a target directory
	bool FileCopy(const std::string& source_file_path, const std::string& target_dir_path); 

	//copies a directory and the files/directories that it contains
	bool CopyDir(const std::string& source_dir_path, const std::string& target_dir_path);

	//copies a symlink as a symlink (not the files they point to)
	bool CopySymLink(const std::string& source_sym_link_path, const std::string& target_dir_path);

	// *****************************************


	// Delete functions *****************************************
	
	//removes the given directory and all its contents
	bool DeleteDir(const std::string& dir_path);

	//removes a single file, symlinks are not followed (the symlink is deleted, not the target file)
	bool FileRemove(const std::string& file_path);

	// *****************************************


	// Create dir function *****************************************
	bool CreateDir(const std::string& parent_dir_path, const std::string& new_dir_name);

	// Creates all directiories from the lowest to the first that already exists
	bool RecursiveDirCreation(const std::string& new_dir_path);
	// *****************************************


	// Create dir function *****************************************

	// Renames a given file from a path to another path
	bool FileRename(const std::string& source_file_path, const std::string& target_dir_path);

	//Function for error handling message 
	void ThrowRuntimeExc(const std::string& message);

	// *****************************************
}