#pragma once
#include <iostream>
#include <ctime>
#include <string> 
#include <fstream>
#include <sstream>
#include <filesystem>
#include "FileHandler.h"


class Logger
{
private: std::string fileName;

public:
	Logger();
	void SetFileName(std::string fileName);

	// This function returns the current date and time
	std::string getDate();
	//This function creates a new text file
	void createFile(const std::string& log_dir_path);
	//This function writes information or error messages to a text file
	void writingFunction(int opt, std::string comm);
};

