#pragma once
#include <iostream>

namespace lr
{
	// Function that checks if the given username respects the validation rules
	bool ValidateUsername(const std::string& username);
}