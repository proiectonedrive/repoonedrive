#pragma once

#include <cassert>
#include <iostream>
#include <chrono>
#include <iomanip>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <string>

namespace metadata
{
	////////////////////////
	// The FNV hash functions
	////////////////////////
	///
	
	/// Const values for the hash generation
	const uint32_t Seed = 0x811C9DC5; // 2166136261
	const uint32_t Prime = 0x01000193; //   16777619

	// The main hash function
	inline uint32_t fnv1a(const void* data, size_t numBytes, uint32_t hash = Seed);

	/// Helper function to hash an std::string
	inline uint32_t fnv1a(const std::string& text, uint32_t hash = Seed);

	////////////////////////

	// Function that returns an int value representing the last modified date of a file/directory
	int GetLastWriteTime(const std::filesystem::path _target_path);

	// Function that converts the file data to a string
	std::string fileToString(const std::filesystem::path& _target_path);

	// Function that iterates through a directory and generates a
	// vector of pairs that contains the path of the file and the
	// generated hash through the FNV hash function (std::vector<std::pair<std::string, uint32_t>>)
	void IterateDirectoryHash(const std::filesystem::path _target_path, std::unordered_map<std::string, std::string>& current_file_map);
}
