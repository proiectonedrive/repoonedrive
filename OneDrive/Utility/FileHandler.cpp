#include "FileHandler.h"
#include <iostream>
#include <fstream>
#include <stack>

// Copy functions *****************************************

bool fh::FileCopy(const std::string& source_file_path, const std::string& target_dir_path)
{
	//std::filesystem functions take std::filesystem::path as parameters, so we will create
	//2 path objects so that only one conversion will be needed

	std::filesystem::path _source_file_path = source_file_path;
	std::filesystem::path _target_path = target_dir_path;
	std::string error_message = "";
	
	
	if (!std::filesystem::exists(_source_file_path))
		error_message="Source path is invalid";
	else if (!std::filesystem::exists(_target_path))
		error_message="Target path is invalid";
	else if (std::filesystem::is_directory(_source_file_path))
		error_message="Source path must lead to a file, not a directory";
	else if (!std::filesystem::is_directory(_target_path))
		error_message="Target path must lead to a directory";
	else if (std::filesystem::is_symlink(_source_file_path))
		error_message="Source path must lead to a file, not a symlink";
	
	if (error_message.length())
	{
		fh::ThrowRuntimeExc("[Error]: FileHandler -> FileCopy -> " + error_message
			+ ":\n source: \"" + source_file_path + "\"\n target: \"" + target_dir_path + "\"");
		return false;
	}
	
	try
	{
		_target_path /= _source_file_path.filename();//specifying the name of the new copy file
		std::filesystem::copy_file(_source_file_path, _target_path,
			std::filesystem::copy_options::overwrite_existing);//copies the file overwriting existing files with the same name
	}
	catch (const std::filesystem::filesystem_error& except)
	{
		throw std::runtime_error("[Error]: FileHandler -> FileCopy -> " + std::string(except.what()));
		return false;
	}
	return true;
}

bool fh::CopyDir(const std::string& source_dir_path, const std::string& target_dir_path)
{
	std::filesystem::path _source_dir_path = source_dir_path;
	std::filesystem::path _target_path = target_dir_path;
	std::string error_message = "";
	
	if (!std::filesystem::exists(_source_dir_path))
		error_message="Source path is invalid";
	else if (!std::filesystem::exists(_target_path))
		error_message="Target path is invalid";
	else if (!std::filesystem::is_directory(_source_dir_path))
		error_message="Source path must lead to a directory";
	else if (!std::filesystem::is_directory(_target_path))
		error_message="Target path must lead to a directory";
	
	if (error_message.length())
	{
		fh::ThrowRuntimeExc("[Error]: FileHandler -> CopyDir -> " + error_message
			+ ":\n source: \"" + source_dir_path + "\"\n target: \"" + target_dir_path + "\"");
		return false;
	}

	_target_path /= _source_dir_path.filename();//specifying the name of the new copy file

	//copies the directory and its files and subdirectories, overwriting existing files
	//symlinks are copied as symlinks (not the files they point to)

	try
	{
		std::filesystem::copy(_source_dir_path, _target_path,
			std::filesystem::copy_options::overwrite_existing
			| std::filesystem::copy_options::recursive
			| std::filesystem::copy_options::copy_symlinks);
	}
	catch(std::filesystem::filesystem_error except)
	{
		throw std::runtime_error("[Error]: FileHandler -> CopyDir -> " + std::string(except.what()));
		return false;
	}

	return true;
}

bool fh::CopySymLink(const std::string& source_sym_link_path, const std::string& target_dir_path)
{
	std::filesystem::path _source_sym_link_path = source_sym_link_path;
	std::filesystem::path _target_path = target_dir_path;
	std::string error_message = "";

	
	if (!std::filesystem::is_symlink(_source_sym_link_path))
		error_message="Source path is invalid";
	else if (!std::filesystem::exists(_target_path))
		error_message="Target path is invalid";
	else if (!std::filesystem::is_directory(_target_path))
		error_message="Target path must lead to a directory";
	
	
	if(error_message.length())
	{
	fh::ThrowRuntimeExc("[Error]: FileHandler -> CopySymLink -> " + error_message
			+ ":\n source: \"" + source_sym_link_path + "\"\n target: \"" + target_dir_path + "\"");
		return false;
	}

	_target_path /= _source_sym_link_path.filename();//specifying the name of the new copy file

	try
	{
		std::filesystem::copy_symlink(_source_sym_link_path, _target_path);
	}
	catch(const std::filesystem::filesystem_error& except)
	{
		throw std::runtime_error("[Error]: FileHandler -> CopySymLink -> " + std::string(except.what()));
		return false;
	}

	return true;
}

// *****************************************


// Delete functions *****************************************

bool fh::DeleteDir(const std::string& dir_path)
{
	std::filesystem::path _dir_path = dir_path;
	std::string error_message = "";

	if (!std::filesystem::exists(_dir_path))
		error_message="Path is invalid";
	else if (!std::filesystem::is_directory(_dir_path))
		error_message="Path must lead to a directory";
	
	if (error_message.length())
	{
		fh::ThrowRuntimeExc("[Error]: FileHandler -> DeleteDir -> " + error_message
			+ ":\n path: \"" + dir_path + "\"");
		return false;
	}

	try
	{
		std::filesystem::remove_all(_dir_path);
	}
	catch (const std::filesystem::filesystem_error& except)
	{
		throw std::runtime_error("[Error]: FileHandler -> DeleteDir -> " + std::string(except.what()));
		return false;
	}

	return true;
}

bool fh::FileRemove(const std::string& file_path)
{
	std::filesystem::path _file_path = file_path;
	std::string error_message = "";
	
		if (!std::filesystem::exists(_file_path) && !std::filesystem::is_symlink(_file_path))
			error_message="Path is invalid";
		else if (std::filesystem::is_directory(_file_path))
			error_message="Path leads to a directory";

	if (error_message.length())
	{
		fh::ThrowRuntimeExc("[Error]: FileHandler -> FileRemove -> " + error_message
			+ ":\n path: \"" + file_path + "\"");
		return false;
	}

	try
	{
		std::filesystem::remove(_file_path);
	}
	catch (const std::filesystem::filesystem_error& except)
	{
		throw std::runtime_error("[Error]: FileHandler -> FileRemove -> " + std::string(except.what()));
		return false;
	}

	return true;
}

// *****************************************


// Create dir function *****************************************
bool fh::CreateDir(const std::string& parent_dir_path, const std::string& new_dir_name)
{
	std::filesystem::path _parent_dir_path = parent_dir_path;
	
	
	std::string error_message="";
	 if (!std::filesystem::exists(_parent_dir_path))
		error_message="Parent directory path is invalid";
	 else if (!std::filesystem::is_directory(_parent_dir_path))
		error_message = "Parent path must lead to a directory";
	

	 if (error_message.length())
	 {
		 fh::ThrowRuntimeExc("[Error]: FileHandler -> CreateDir -> " + error_message
			 + ":\n parent path: \"" + parent_dir_path + "\"");
		 return false;
	 }


	std::filesystem::path _new_dir_path = _parent_dir_path / new_dir_name;

	try
	{
		std::filesystem::create_directory(_new_dir_path); //creating the new dir
	}
	catch (const std::filesystem::filesystem_error& except)
	{
		throw std::runtime_error("[Error]: FileHandler -> CreateDir -> " + std::string(except.what()));
		return false;
	}

	return true;
}

bool fh::RecursiveDirCreation(const std::string& new_dir_path)
{
	//stack contains the path of the parent directory on the first position and the name of the new directory on the second
	std::stack<std::pair<std::string, std::string>> dir_creation_stack;
	std::filesystem::path current_path(new_dir_path);

	//find what directories need to be created
	for (/*empty*/; !std::filesystem::exists(current_path) && current_path.string().length(); 
		current_path = current_path.parent_path())
		dir_creation_stack.push(std::make_pair(current_path.parent_path().string(), current_path.filename().string()));

	for (/*empty*/; !dir_creation_stack.empty(); dir_creation_stack.pop())
	{
		if (!CreateDir(dir_creation_stack.top().first, dir_creation_stack.top().second))
			fh::ThrowRuntimeExc("[Warning]: FileHandler -> RecursiveDirCreation -> Could not create directory: \""
				+ dir_creation_stack.top().first + "\\" + dir_creation_stack.top().second + "\"");
	}

	return true;
}

// *****************************************

// Rename function *****************************************
bool fh::FileRename(const std::string& source_file_path, const std::string& target_dir_path)
{
	//std::filesystem functions take std::filesystem::path as parameters, so we will create
	//2 path objects so that only one conversion will be needed

	std::filesystem::path _source_file_path = source_file_path;
	std::filesystem::path _target_path = target_dir_path;

	std::string error_message="";
		
	if (!std::filesystem::exists(_source_file_path))
			error_message="Source path is invalid";
		else if (std::filesystem::is_directory(_source_file_path))
		    error_message = "Source path must lead to a file, not a directory";
		else if (std::filesystem::is_symlink(_source_file_path))
		    error_message = "Source path must lead to a file, not a symlink";
	
	
	if (error_message.length())
	{
		fh::ThrowRuntimeExc("[Error]: FileHandler->FileRename->"+ error_message+ ":\n source:\ " + source_file_path + "\"\n target: \"" + target_dir_path + "\"");
		return false;
	}
	

	try
	{
		std::filesystem::rename(_source_file_path, _target_path);
	}
	catch (const std::filesystem::filesystem_error& except)
	{
		throw std::runtime_error("[Error]: FileHandler -> FileRename -> " + std::string(except.what()));
		return false;
	}
	return true;
}


void fh::ThrowRuntimeExc(const std::string& message)
{
	throw std::runtime_error(message);
}