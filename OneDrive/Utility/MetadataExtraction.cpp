#include "MetadataExtraction.hpp"


// NOTE: Prime and hash values are defined in the header file! (Prime, Seed)
// The main hash function
uint32_t metadata::fnv1a(const void* data, size_t numBytes, uint32_t hash)
{
	assert(data);
	const unsigned char* ptr = static_cast<const unsigned char*>(data);
	while (numBytes--)
		hash = (*ptr++ ^ hash) * Prime;
	return hash;
}

/// Helper function to hash an std::string
uint32_t metadata::fnv1a(const std::string& text, uint32_t hash)
{
	return fnv1a(text.c_str(), text.length(), hash);
}

// Function that returns an int value representing the last modified date of a file/directory
int metadata::GetLastWriteTime(const std::filesystem::path _target_path)
{
	if (!std::filesystem::exists(_target_path))
	{
		throw std::exception("The path given does not exist!\n");
		return false;
	}
	const auto fileTime = std::filesystem::last_write_time(_target_path);
	const auto systemTime = std::chrono::clock_cast<std::chrono::system_clock>(fileTime);
	const auto time = std::chrono::system_clock::to_time_t(systemTime);

	return time;
}

// Function that converts the file data to a string
std::string metadata::fileToString(const std::filesystem::path& _target_path)
{
	if (std::filesystem::exists(_target_path))
	{
		std::ifstream f(_target_path, std::ios::binary); //taking file as inputstream
		std::string str;
		if (f) {
			std::ostringstream ss;
			ss << f.rdbuf(); // reading data
			str = ss.str();
			str = std::to_string(GetLastWriteTime(_target_path)) + str;
		}
		return str;
	}
	else
		throw std::exception("This file does not exist");
}


// Function that iterates through a directory and generates a
// unordered map with the form of (path, hashed content of the file with that path)
void metadata::IterateDirectoryHash(const std::filesystem::path _target_path, std::unordered_map<std::string, std::string>& current_file_map)
{
	if (std::filesystem::exists(_target_path) && std::filesystem::is_directory(_target_path))
		for (const auto& dirEntry : std::filesystem::recursive_directory_iterator(_target_path))
		{
			if(!std::filesystem::is_directory(dirEntry))
			{
				std::string full_metadata = dirEntry.path().string().substr(_target_path.string().length() + 1) + std::to_string(dirEntry.file_size()) + std::to_string(GetLastWriteTime(dirEntry));

				//current_file_map[std::to_string(fnv1a(full_metadata))] = dirEntry.path().string().substr(_target_path.string().length() + 1);
				current_file_map[std::to_string(fnv1a(fileToString(dirEntry)))] = dirEntry.path().string().substr(_target_path.string().length() + 1);
			}
			//else
			//{
				//std::string full_metadata = dirEntry.path().string().substr(_target_path.string().length() + 1) + std::to_string(dirEntry.file_size()) + std::to_string(GetLastWriteTime(dirEntry));

				//current_file_map[std::to_string(fnv1a(full_metadata))] = dirEntry.path().string().substr(_target_path.string().length() + 1);
			//}
		}
	else
		throw std::exception("The path does not point to a valid directory!");

	return;
}
