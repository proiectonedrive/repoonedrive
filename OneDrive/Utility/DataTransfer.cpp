#include "DataTransfer.h"
#include <memory>

#define max_file_type_code_size 3

namespace DataTransfer
{
	//Basic transfer functions **************************************
	int64_t RecvBuffer(SOCKET sock, char* buffer, const uint64_t& buffer_size,
		const uint32_t& chunk_size)
	{ 
		uint64_t total_recieved = 0;
		
		while (total_recieved < buffer_size)
		{
			const int32_t recieved = recv(sock, &buffer[total_recieved],
				std::min<uint32_t>(chunk_size, static_cast<uint32_t>(buffer_size - total_recieved)), 0);
			if (recieved < 0)// this is an error
				return -1;

			total_recieved += recieved;
		}
		return total_recieved;
	}

	std::string RecvText(SOCKET sock)
	{
		std::unique_ptr<char[]> buffer(new char[4096]);
		//recieve message size
		
		if (RecvBuffer(sock, buffer.get(), maximum_size_digit_count) < 0)
		{
			throw std::exception("[Error]: DataTransfer -> RecvText -> Failed to recieve text lenght");
			return "error";
		}

		uint64_t message_size = std::stoull(buffer.get());

		if (RecvBuffer(sock, buffer.get(), message_size) < 0)
		{
			throw std::exception("Error]: DataTransfer -> RecvText -> Failed to recieve text");
			return "error";
		}

		buffer[message_size] = NULL;
		std::string text(buffer.get());

		return text;
	}

	int64_t SendBuffer(SOCKET sock, const char* buffer,
		const uint64_t& buffer_size, const uint32_t& chunk_size)
	{
		uint64_t total_sent = 0;

		while (total_sent < buffer_size) {
			const int32_t sent = send(sock, &buffer[total_sent],
				std::min<uint32_t>(chunk_size, static_cast<uint32_t>(buffer_size - total_sent)), 0);
			if (sent < 0)
				return -1;
			total_sent += sent;
		}

		return total_sent;
	}

	int64_t SendText(SOCKET sock, const std::string& text)
	{
		//sending the size of the message
		if (DataTransfer::SendBuffer(sock, DataTransfer::StandardizeSizeString(text.size()).data(),
			maximum_size_digit_count) < 0)
		{
			throw std::exception("[Error]: DataTransfer -> SendText -> Failed to send the size of the text");
			return -1;
		}

		//sending the message
		int64_t result = DataTransfer::SendBuffer(sock, text.data(), text.size());
		if (result < 0)
		{
			throw std::exception("[Error]: DataTransfer -> SendText -> Failed to send the text");
			return -1;
		}

		return result;
	}


	// File transfer functions **************************************
	int64_t RecvFile(SOCKET sock, const std::string& working_dir_path, const uint64_t& chunk_size)
	{
		std::string file_name;
		try
		{
			file_name = working_dir_path + "\\" + RecvText(sock);//recieving file name and building local path
		}
		catch (const std::exception& except)
		{
			throw std::runtime_error("[Error] DataTransfer -> RecvFile -> Failed to recieve file path --> " 
				+ std::string(except.what()));
			return -1;
		}

		//recieve file path
		{
			//if the file path does now exist, it will be created
			std::filesystem::path file_path(file_name);
			if (!std::filesystem::exists(file_path.parent_path()))
			{
				try
				{
					fh::RecursiveDirCreation(file_path.parent_path().string());
				}
				catch (const std::exception& except)
				{
					throw std::runtime_error("[Error] DataTransfer -> RecvFile -> Failed to create additional directories --> "
						+ std::string(except.what()));
					return -1;
				}
			}
		}

		//recieve file size
		int64_t file_size;
		{
			std::unique_ptr<char[]> size_buffer(new char[maximum_size_digit_count + 1]);
			size_buffer[maximum_size_digit_count] = NULL;

			if (RecvBuffer(sock, size_buffer.get(), maximum_size_digit_count) < 0)//recieving the size of the file
			{
				throw std::exception("[Error]: DataTransfer -> RecvFile -> Failed to recieve file size");
				return -1;
			}

			file_size = std::stoull(size_buffer.get());
		}
		
		std::ofstream file(file_name, std::ofstream::binary);
		std::unique_ptr<char[]> buffer(new char[chunk_size]);
		int64_t remaining = file_size;
		while (remaining != 0)
		{
			const int64_t size_recieved = RecvBuffer(sock, buffer.get(),
				(int)__min(remaining, (int64_t)chunk_size));
			
			if (size_recieved < 0 || !file.write(buffer.get(), size_recieved))
			{
				file.close();
				throw std::exception("[Error]: DataTransfer -> RecvFile -> Failed to recieve complete file contents");
			}
			remaining -= size_recieved;
		}

		file.close();
		return file_size;
	}

	int64_t RecvDir(SOCKET sock, const std::string& working_dir_path)
	{
		uint16_t buffer_size = 1024;
		std::unique_ptr<char[]> buffer(new char[buffer_size]);

		//recieving the number of files that will be sent
		if (DataTransfer::RecvBuffer(sock, buffer.get(), maximum_size_digit_count) < 0) 
			throw std::exception("[Error] DataTransfer -> RecvDir -> Failed to recieve the number of files");

		uint64_t files_to_be_recieved_count = std::stoull(buffer.get());
		memset(buffer.get(), 0, buffer_size);
		
		for (uint64_t recieved_file_count = 0; recieved_file_count < files_to_be_recieved_count; recieved_file_count++)
		{
			CommunicationCodes::FileTypeCodes file_type_code = RecvFileTypeCode(sock);

			if (file_type_code == CommunicationCodes::FileTypeCodes::REGULAR_FILE) //recieving a regular file
			{
				try
				{
					DataTransfer::RecvFile(sock, working_dir_path);
				}
				catch (const std::exception& except)
				{
					throw std::runtime_error("[Error] DataTransfer -> RecvDir -> Failed to recieve regular file --> "
					+ std::string(except.what()));
				}
			}
			else if(file_type_code == CommunicationCodes::FileTypeCodes::DIRECTORY)
			{
				try
				{
					std::filesystem::path new_dir_relative_path(DataTransfer::RecvText(sock));

					//building the local path and creating a directory
					fh::CreateDir(working_dir_path + "\\" + new_dir_relative_path.parent_path().string(),
						new_dir_relative_path.filename().string());
				}
				catch (const std::exception& except)
				{
					throw std::runtime_error("[Error] DataTransfer -> RecvDir -> Failed to recieve directory --> "
						+ std::string(except.what()));
				}
			}
			memset(buffer.get(), 0, buffer_size);
		}
	
		return files_to_be_recieved_count;
	}

	std::string StandardizeSizeString(const uint64_t& size)
	{
		//all sizes will be sent under a 20 digit standard so that the server is able to easily decode it
		std::string format_file_size_char = "00000000000000000000";//20 zeros (max uint64_t has 20 digits)
		std::string file_size_char = std::to_string(size);
		format_file_size_char.replace(format_file_size_char.begin() + format_file_size_char.size() - file_size_char.size(),
			format_file_size_char.end(), file_size_char.data());

		return format_file_size_char;
	}

	uint64_t GetFileSize(const std::string& file_name)
	{
		std::ifstream file(file_name, std::ios::binary);

		file.seekg(0, std::ios::end);
		uint64_t file_size = file.tellg(); //getting file size
		file.close();

		return file_size;
	}

	int64_t SendFile(SOCKET sock, const std::string& file_path, const std::string& working_dir_path, const uint32_t& chunk_size)
	{
		std::string local_file_path = working_dir_path + "\\" + file_path;

		uint64_t file_size = GetFileSize(local_file_path);//getting file size
		
		if (file_size < 0)
		{
			throw std::runtime_error("[Error]: DataTransfer -> SendFile -> Failed to read file size for file: "
				+ local_file_path);
			return -1;
		}

		//sending the file name so the server knows how to name the copy
		try
		{
			SendText(sock, file_path);
		}
		catch (const std::exception& except)
		{
			throw std::runtime_error("[Error]: DataTransfer -> SendFile -> Failed to send file name --> "
				+ std::string(except.what()));
		}

		//sending the size of the file
		if (SendBuffer(sock, StandardizeSizeString(file_size).data(), maximum_size_digit_count) < 0)
			throw std::exception("[Error]: DataTransfer -> SendFile -> Failed to send file size");

		std::ifstream file(local_file_path, std::ifstream::binary);
		std::unique_ptr<char[]> buffer(new char[chunk_size]);
		uint64_t remaining = file_size;

		while (remaining != 0)
		{
			const uint32_t size_to_be_sent = static_cast<uint32_t>(std::min<uint64_t>(remaining, chunk_size));
			//min will always be lower than maximum uint32_t
			if (!file.read(buffer.get(), size_to_be_sent))
				break;

			const int64_t sent = SendBuffer(sock, buffer.get(), size_to_be_sent);
			if (sent < 0)
			{
				file.close();
				throw std::exception("[Error]: DataTransfer -> SendFile -> Failed to send complete file contents");
			}

			remaining -= sent;
		}

		file.close();
		return file_size;
	}

	int64_t SendDir(SOCKET sock, const std::string& dir_path)
	{
		std::vector<std::string> paths;

		for (const auto& dir_entry : std::filesystem::recursive_directory_iterator{ dir_path })
			paths.push_back(dir_entry.path().string());
		
		//sending the number of files that will be sent
		if (SendBuffer(sock, StandardizeSizeString(paths.size()).data(), maximum_size_digit_count) < 0)
			throw std::exception("[Error]: DataTransfer -> SendDir -> Failed to send the file count");

		//needed to convert connection codes enum to char in order to be sent
		std::unique_ptr<char[]> buffer(new char[max_file_type_code_size]);
		for (std::string path : paths)
		{
			if (!std::filesystem::is_directory(path))
			{
				try
				{
					SendFileTypeCode(sock, CommunicationCodes::FileTypeCodes::REGULAR_FILE);
					SendFile(sock, path.substr(dir_path.length() + 1), dir_path);
				}
				catch (const std::exception& except)
				{
					throw std::runtime_error("[Error]: DataTransfer -> SendDir -> Failed to send file type for: \""
						+ path + "\" --> " + std::string(except.what()));
				}
			}
			else
			{
				//formatting the path of the directory so it is relative to the one drive directory
				std::string relative_file_path = path.substr(dir_path.length() + 1);

				//sending the dir name so the server knows how to name the copy
				try
				{
					SendFileTypeCode(sock, CommunicationCodes::FileTypeCodes::DIRECTORY);
					SendText(sock, relative_file_path);
				}
				catch (const std::exception& except)
				{
					throw std::runtime_error("[Error]: DataTransfer -> SendDir -> Failed to send directory path for directory \"" +
						path + "\" --> " + except.what());
				}
			}
			memset(buffer.get(), 0, max_file_type_code_size);
		}
		return paths.size();
	}


	// Communication codes transfer functions *****************************************
	bool SendLoginCode(SOCKET sock, const CommunicationCodes::LoginCodes& code)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size]);
		std::to_chars(buffer.get(), buffer.get() + maximum_code_size - 1, static_cast<int>(code));
		if (DataTransfer::SendBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to send login/register code");

		return true;
	}
	bool SendConnectionHandlerCode(SOCKET sock, const CommunicationCodes::ConnectionHandlerCodes& code)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size]);
		std::to_chars(buffer.get(), buffer.get() + maximum_code_size - 1, static_cast<int>(code));
		if (DataTransfer::SendBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to send connection handler action code");

		return true;
	}
	bool SendFileTypeCode(SOCKET sock, const CommunicationCodes::FileTypeCodes& code)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size]);
		std::to_chars(buffer.get(), buffer.get() + maximum_code_size - 1, static_cast<int>(code));
		if (DataTransfer::SendBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to send file type code");

		return true;

	}
	bool SendAnswersCode(SOCKET sock, const CommunicationCodes::AnswersCodes& code)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size]);
		std::to_chars(buffer.get(), buffer.get() + maximum_code_size - 1, static_cast<int>(code));
		if (DataTransfer::SendBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to send answer code");

		return true;
	}

	CommunicationCodes::LoginCodes RecvLoginCode(SOCKET sock)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size + 1]);
		CommunicationCodes::LoginCodes login_register;
		if (DataTransfer::RecvBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to recieve login/register code");

		login_register = static_cast<CommunicationCodes::LoginCodes>(std::stoi(buffer.get()));
		return login_register;
	}
	CommunicationCodes::ConnectionHandlerCodes RecvConnectionHandlerCode(SOCKET sock)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size + 1]);
		CommunicationCodes::ConnectionHandlerCodes action_type;
		if (DataTransfer::RecvBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to recieve connection handler action code");

		action_type = static_cast<CommunicationCodes::ConnectionHandlerCodes>(std::stoi(buffer.get()));
		return action_type;
	}
	CommunicationCodes::FileTypeCodes RecvFileTypeCode(SOCKET sock)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size + 1]);
		CommunicationCodes::FileTypeCodes file_type;
		if (DataTransfer::RecvBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to recieve file type code");

		file_type = static_cast<CommunicationCodes::FileTypeCodes>(std::stoi(buffer.get()));
		return file_type;
	}
	CommunicationCodes::AnswersCodes RecvAnswersCode(SOCKET sock)
	{
		std::unique_ptr<char[]> buffer(new char[maximum_code_size + 1]);
		CommunicationCodes::AnswersCodes answer;
		if (DataTransfer::RecvBuffer(sock, buffer.get(), maximum_code_size) < 0)
			throw std::exception("[Error]: Failed to recieve answer code");

		answer = static_cast<CommunicationCodes::AnswersCodes>(std::stoi(buffer.get()));
		return answer;
	}
}