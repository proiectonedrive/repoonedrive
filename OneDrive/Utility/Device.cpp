#include <iostream>
#include "Device.h"

std::string Device::getDeviceName()
{

	TCHAR compname[UNCLEN + 1];
	DWORD compname_len = UNCLEN + 1;
	GetComputerName((TCHAR*)compname, &compname_len);
	std::wstring test = compname;
	std::string test2(test.begin(), test.end());
	return test2;
}
