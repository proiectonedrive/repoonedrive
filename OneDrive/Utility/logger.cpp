#include "Logger.h"
#include <iostream>
#include <ctime>
#include <string> 
#include <fstream>
#include <sstream>
#include <filesystem>


Logger::Logger()
{
}

void Logger::SetFileName(std::string fileName)
{
    this->fileName = fileName;
}
// This function returns the current date and time
std::string Logger::getDate()
{
    
    struct tm timeinfo;
    std::time_t crtTime = std::time(nullptr);
    auto error = ::localtime_s(&timeinfo, &crtTime);
    std::stringstream sstream;
    sstream << '[' << std::put_time(&timeinfo, "%Y-%m-%d %H:%M:%S") << ']';
    std::string date= sstream.str();

    return  date;

}
//This function creates a new text file
void Logger::createFile(const std::string& log_dir_path)
{
    int file_count = 0;
    if (!std::filesystem::exists(log_dir_path))
    {
        fh::CreateDir(".", log_dir_path);
    }

    for (const auto& dir_entry : std::filesystem::recursive_directory_iterator{ log_dir_path })
        file_count++;
    fileName = log_dir_path + "\\log_file" + std::to_string(file_count) + ".txt";
    std::ofstream w(fileName);

    w.close();
}

void Logger::writingFunction(int opt, std::string comm)
{
    std::ofstream write;
    if (fileName == " ")
        throw("the file dosen't exist");
    write.open(fileName, std::ofstream::app);
    if (opt == 0)
    {
        write << "[INFO ]";
    }
    else
        write << "[ERROR]";

    write << getDate() << comm << std::endl;
    write.close();
}
