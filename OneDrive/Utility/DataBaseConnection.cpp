#include <iostream>
#include "DataBaseConnection.hpp"




//This function creates the connection to the database
int DataBaseConnection::createDB(const char* db_path)
{
	sqlite3* DB;
	int exit = 0;
	exit = sqlite3_open(db_path, &DB);
	sqlite3_close(DB);

	return 0;
}

//This function creates a table in the database
int DataBaseConnection::createTable(const char* db_path, std::string sql)
{
	sqlite3* DB;
	char* messageError;

	try
	{
		int exit = 0;
		exit = sqlite3_open(db_path, &DB);
		// An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here 
		exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
		if (exit != SQLITE_OK) {
			std::cerr << "Error in createTable function." << std::endl;
			sqlite3_free(messageError);
		}
		else
			std::cout << "Table created Successfully" << std::endl;
		sqlite3_close(DB);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what();
	}

	return 0;
	
}
//This function inserts data into the table
bool DataBaseConnection::insertData(const char* db_path,std::string sql)
{
	sqlite3* DB;
	char* messageError;
    
	int exit = sqlite3_open(db_path, &DB);
	// An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here 
	exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error in insertData function." << std::endl;
		sqlite3_free(messageError);
		return false;
		
	}
	else
		std::cout << "Records inserted Successfully!" << std::endl;
	return true;
}

//This function updates the data in a table
int DataBaseConnection::updateData(const char* db_path, std::string sql)
{
	sqlite3* DB;
	char* messageError;


	int exit = sqlite3_open(db_path, &DB);
	// An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here 
	exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &messageError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error in updateData function." << std::endl;
		sqlite3_free(messageError);
	}
	else
		std::cout << "Records updated Successfully!" << std::endl;

	
	return 0;
}
//This function selects data from the table
int DataBaseConnection::selectData(const char* db_path, std::string sql)
{
	sqlite3* DB;
	char* messageError;

	int exit = sqlite3_open(db_path, &DB);
	//An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here
	exit = sqlite3_exec(DB, sql.c_str(), callback, NULL, &messageError);

	if (exit != SQLITE_OK) {
		std::cerr << "Error in selectData function." << std::endl;
		sqlite3_free(messageError);
	}
	else
		std::cout << "Records selected Successfully!" <<std:: endl;

	return 0;
}
// retrieve contents of database used by selectData()
// argc: holds the number of results, argv: holds each value in array, azColName: holds each column returned in array
int DataBaseConnection::callback(void* NotUsed, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++) {
		// column name and value
		std::cout << azColName[i] << ": " << argv[i] << std::endl;
	}

	std::cout << std::endl;

	return 0;
}
//This function deletes data from the table
int DataBaseConnection::deleteData(const char* db_path, std::string sql)
{
	sqlite3* DB;
	char* messageError;

	int exit = sqlite3_open(db_path, &DB);
	/* An open database, SQL to be evaluated, Callback function, 1st argument to callback, Error msg written here */
	exit = sqlite3_exec(DB, sql.c_str(), callback, NULL, &messageError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error in deleteData function." << std::endl;
		sqlite3_free(messageError);
	}
	else
		std::cout << "Records deleted Successfully!" << std::endl;

	
	return 0;
}


//This function selects path and hash data from the table (file)and places them in an unorderd_map (file_map)
int DataBaseConnection::selectFile(const char* db_path, std::unordered_map < std::string, std::string >& file_map, int id_user)
{
	sqlite3* db;
	sqlite3_stmt* stmt{};


	if (sqlite3_open(db_path, &db) == SQLITE_OK)
	{
		sqlite3_prepare_v2(db, "SELECT path,hash FROM file WHERE id_user = ?;", -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, id_user);
		sqlite3_step(stmt);
		while (sqlite3_column_text(stmt, 0))
		{
			file_map[std::string((char*)sqlite3_column_text(stmt, 1))] = std::string((char*)sqlite3_column_text(stmt, 0));

			sqlite3_step(stmt);
		}


	}
	else
	{
		std::cout << "Failed to open db\n";
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);



	return 0;
}
 
bool DataBaseConnection::findUser(const char* db_path, const std::string& username)
{
	sqlite3* db;
	sqlite3_stmt* stmt{};


	if (sqlite3_open(db_path, &db) == SQLITE_OK)
	{
		std::string sql_command = "SELECT username FROM user WHERE username = ?;";
		sqlite3_prepare_v2(db, sql_command.c_str(), -1, &stmt, NULL);
		sqlite3_bind_text(stmt, 1, username.c_str(), username.length(), SQLITE_TRANSIENT);
		sqlite3_step(stmt);
		if (sqlite3_column_text(stmt, 0))
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			return true;
		}
	}
	else
		std::cout << "Failed to open db\n";

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return false;
}

bool DataBaseConnection::findDevice(const char* db_path, const std::string& username,const std::string& device_name)
{

	sqlite3* db;
	sqlite3_stmt* stmt{};

	int id_user = DataBaseConnection::getUserId(db_path, username);

	if (sqlite3_open(db_path, &db) == SQLITE_OK)
	{
		std::string sql_command = "SELECT device_name FROM device WHERE id_user = ? AND device_name = ?";
		sqlite3_prepare_v2(db, sql_command.c_str(), -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, id_user);
		sqlite3_bind_text(stmt, 2, device_name.c_str(), device_name.length(), SQLITE_TRANSIENT);
		sqlite3_step(stmt);
		if (sqlite3_column_text(stmt, 0))
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			return true;
		}
	}
	else
		std::cout << "Failed to open db\n";

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return false;
}

int DataBaseConnection::getUserId(const char* db_path, const std::string& username)
{
	sqlite3* db;
	sqlite3_stmt* stmt{};


	if (sqlite3_open(db_path, &db) == SQLITE_OK)
	{
		std::string sql_command = "SELECT id_user, username FROM user WHERE username = ?;";
		sqlite3_prepare_v2(db, sql_command.c_str(), -1, &stmt, NULL);
		sqlite3_bind_text(stmt, 1, username.c_str(), username.length(), SQLITE_TRANSIENT);
		sqlite3_step(stmt);
		if (sqlite3_column_text(stmt, 0))
		{
			int user_id = stoi(std::string((char*)sqlite3_column_text(stmt, 0)));
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			return user_id;
		}
	}
	else
		std::cout << "Failed to open db\n";

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return false;
}

std::string DataBaseConnection::getFileHash(const char* db_path, const std::string& path, int id_user)
{
	sqlite3* db;
	sqlite3_stmt* stmt{};


	if (sqlite3_open(db_path, &db) == SQLITE_OK)
	{
		std::string sql_command = "select hash from file where id_user = ? and path = ?;";
		sqlite3_prepare_v2(db, sql_command.c_str(), -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, id_user);
		sqlite3_bind_text(stmt, 2, path.c_str(), path.length(), SQLITE_TRANSIENT);
		sqlite3_step(stmt);
		if (sqlite3_column_text(stmt, 0))
		{
			std::string hash = std::string((char*)sqlite3_column_text(stmt, 0));
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			return hash;
		}
	}
	else
		std::cout << "Failed to open db\n";

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return "";
}