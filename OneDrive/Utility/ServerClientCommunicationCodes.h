#pragma once
#define maximum_code_size  2
struct CommunicationCodes
{
	enum class LoginCodes
	{
		LOGIN = 1,
		REGISTER,
		DISCONNECT = 9
	};

	enum class ConnectionHandlerCodes
	{
		TEXT_MESSAGE = 1,
		ADD_FILE,
		DIRECTORY,
		DELETE_FILE,
		DOWNLOAD_FILE,
		DOWNLOAD_DIRECTORY,
		RENAME_FILE,
		DISCONNECT = 9
	};
	
	enum class FileTypeCodes
	{
		DIRECTORY = 1,
		REGULAR_FILE,
		SYMLINK
	};
	
	enum class AnswersCodes
	{
		NEGATIVE, POSITIVE
	};
};