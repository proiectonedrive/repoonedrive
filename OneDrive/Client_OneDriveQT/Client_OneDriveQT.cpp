#include "Client_OneDriveQT.h"
#include "DataTransfer.h"
#include <qfiledialog.h>
#include <QMessageBox>

Client_OneDriveQT::Client_OneDriveQT(QWidget* parent)
    : QMainWindow(parent)
{
    Client *client = Client::GetClientInstance();//getting the client instance
    ui.setupUi(this);
    this->setFixedSize(QSize(644, 479));

    SetupWelcomePage();
    SetupLoginPage();
    SetupRegisterPage();
    SetupMainPage();
    SetupSettingsPage();

    //setting up polling timer
    sync_timer = new QTimer();
    sync_timer->setSingleShot(true);

    sync_thread = new SyncThread();
    connect(sync_timer, &QTimer::timeout, this, [&]() { sync_thread->start(); });
    connect(sync_thread, &QThread::finished, this, &Client_OneDriveQT::ThreadFinished);

    //
    ChangePage(PageIndices::WELCOME_PAGE);
}
void Client_OneDriveQT::SetupWelcomePage()
{ 
    connect(ui.GoToLogin, &QPushButton::clicked, this, [&]() { ChangePage(PageIndices::LOGIN_PAGE); });
    connect(ui.GoToRegister, &QPushButton::clicked, this, [&]() { ChangePage(PageIndices::REGISTER_PAGE); });
}
void Client_OneDriveQT::SetupLoginPage()
{
    connect(ui.LoginButton, &QPushButton::clicked, this, [&]() { LoginRegisterClicked(CommunicationCodes::LoginCodes::LOGIN); });
    connect(ui.GoToRegister_2, &QPushButton::clicked, this, [&]() { ChangePage(PageIndices::REGISTER_PAGE); });
    //if config file exists, we use that username to fill the textbox
    {
        std::string config_username = user_config.GetUsername();
        if (config_username.length() > 0)
            ui.input_username->setText(QString::fromStdString(config_username));
    }
}
void Client_OneDriveQT::SetupRegisterPage()
{
    connect(ui.RegisterButton, &QPushButton::clicked, this, [&]() { LoginRegisterClicked(CommunicationCodes::LoginCodes::REGISTER); });
    connect(ui.GoToWelcome, &QPushButton::clicked, this, [&]() { ChangePage(PageIndices::WELCOME_PAGE); });
}
void Client_OneDriveQT::SetupMainPage()
{
    connect(ui.DirTreeView, &QTreeView::clicked, this, &Client_OneDriveQT::ChangeFileView);
    connect(ui.GoToSettings, &QPushButton::clicked, this, [&]() { ChangePage(PageIndices::SETTINGS_PAGE); });
    right_click_file_menu = new QMenu(this);
    delete_file = new QAction("Delete", right_click_file_menu);
    download_file = new QAction("Download", right_click_file_menu);
    upload_file = new QAction("Upload", right_click_file_menu);
    ui.FileTreeView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui.FileTreeView->addAction(delete_file);
    ui.FileTreeView->addAction(download_file);
    ui.FileTreeView->addAction(upload_file);
    connect(delete_file, &QAction::triggered, this, &Client_OneDriveQT::DeleteFile);
    connect(download_file, &QAction::triggered, this, &Client_OneDriveQT::DownloadFile);
    connect(upload_file, &QAction::triggered, this, &Client_OneDriveQT::UploadFile);
    connect(ui.sync_toggle, &QRadioButton::clicked, this, &Client_OneDriveQT::ToggleSync);
    connect(ui.logout, &QPushButton::clicked, this, &Client_OneDriveQT::Logout);
}
void Client_OneDriveQT::SetupSettingsPage()
{
    connect(ui.ChangeDir, &QPushButton::clicked, this, &Client_OneDriveQT::ChangeClientDir);
    connect(ui.BackToMain, &QPushButton::clicked, this, [&]() { ChangePage(PageIndices::MAIN_PAGE); });
    connect(ui.DownloadDir, &QPushButton::clicked, this, &Client_OneDriveQT::DownloadDirectory);
}


void Client_OneDriveQT::InitFileExplorer(const QString& client_dir_path)
{
    InitDirView(client_dir_path);
    InitFileView(client_dir_path);
}
void Client_OneDriveQT::InitDirView(const QString& client_dir_path)
{
    //UI path needs to be one dir up
    QDir parent_path(client_dir_path);
    parent_path.cdUp();
    QString parent_path_str = parent_path.path();

    //dir model
    dir_model = new QFileSystemModel(this);
    dir_model->setRootPath(client_dir_path);
    dir_model->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs);

    //filter model
    dir_filter = new DirTreeFilter();
    dir_filter->SetRootDir(client_dir_path);
    dir_filter->setSourceModel(dir_model);

    //dir view
    ui.DirTreeView->setModel(dir_filter);//setting up the directory view
    for (int column_index = 1; column_index < dir_model->columnCount(); column_index++)//hiding all columns except name
        ui.DirTreeView->hideColumn(column_index);

    ui.DirTreeView->setRootIndex(dir_filter->mapFromSource(dir_model->index(parent_path_str)));//setting up client dir for UI
}
void Client_OneDriveQT::InitFileView(const QString& client_dir_path)
{
    //file model
    file_model = new QFileSystemModel(this);
    file_model->setFilter(QDir::NoDotAndDotDot | QDir::Files);
    file_model->setReadOnly(false);

    //file view
    ui.FileTreeView->setModel(file_model);

    ui.FileTreeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui.FileTreeView->setDragEnabled(true);
    ui.FileTreeView->setAcceptDrops(true);
    ui.FileTreeView->setDropIndicatorShown(true);
    ui.FileTreeView->setDragDropMode(QAbstractItemView::InternalMove);

    for (int column_index = 1; column_index < file_model->columnCount(); column_index++)//hiding all columns except name
        ui.FileTreeView->hideColumn(column_index);

    ui.FileTreeView->setRootIndex(file_model->setRootPath(client_dir_path));
}

void Client_OneDriveQT::LoginRegisterClicked(const CommunicationCodes::LoginCodes& flag)
{
    QString username;
    if(flag == CommunicationCodes::LoginCodes::LOGIN)
		username = ui.input_username->text();
    else
        username = ui.register_text->text();

    Client *client = Client::GetClientInstance();//getting the client instance
    
    if (!client->ConnectToServer()) //connecting to server
    {
        QMessageBox::warning(this, "Connection", "Failed to connect to server!");
        return;
    }

    try
    {
        //we use the return value of the LoginRegister function to determine if this is a new device for the user
        if (!client->LoginRegister(username.toStdString(), flag))
            return;
    }
    catch(const std::exception& except)
    {
        QMessageBox::warning(this, "Login\\Register", except.what());
        return;
    }

    ChangePage(PageIndices::MAIN_PAGE);
    QString client_dir_path = QString::fromStdString(user_config.GetDirPath());
    if(flag == CommunicationCodes::LoginCodes::REGISTER ||
        !client_dir_path.length() || user_config.GetUsername() != username.toStdString())
	    client_dir_path = QFileDialog::getExistingDirectory(); //getting dir location from user

    user_config.SetUserName(username.toStdString());
    user_config.SetDirPath(client_dir_path.toStdString());
    user_config.WriteConfigFile();

	client->SetClientDir(client_dir_path.toStdString());//setting up client dir for sync
	InitFileExplorer(client_dir_path);

    QMessageBox::StandardButton user_answer = QMessageBox::No;
    if (flag == CommunicationCodes::LoginCodes::LOGIN && client->IsDeviceNew())
        user_answer = QMessageBox::question(this, "Server directory", "Do you wish to download the directory form the server?",
            QMessageBox::Yes | QMessageBox::No);

    if (user_answer == QMessageBox::Yes)
        DownloadDirectory();
}
void Client_OneDriveQT::ThreadFinished()
{
    if(ui.sync_toggle->isChecked())
        sync_timer->start(1000);
    
}

void Client_OneDriveQT::ChangePage(const PageIndices& page_index)
{
    ui.stackedWidget->setCurrentIndex(static_cast<unsigned int>(page_index));
    this->repaint();
}
void Client_OneDriveQT::ChangeFileView(const QModelIndex& view_index)
{
    QModelIndex dir_model_index = dir_filter->mapToSource(view_index);
    QString current_path = dir_model->fileInfo(dir_model_index).absoluteFilePath();
    
    //updating file view
    delete file_model;
    InitFileView(current_path);
}


void Client_OneDriveQT::ChangeClientDir()
{
    Client* client = Client::GetClientInstance();
    client->SetClientDir(QFileDialog::getExistingDirectory().toStdString());

    //building new file explorer
    delete dir_model;
    delete dir_filter;
    delete file_model;
    InitFileExplorer(QString::fromStdString(client->GetClientDir()));
}
void Client_OneDriveQT::DeleteFile()
{
    try
    {
        QString file_path = file_model->filePath(ui.FileTreeView->currentIndex());
        fh::FileRemove(file_path.toStdString());
    }
    catch (std::exception except)
    {
        //do something with the error message
    }
}
void Client_OneDriveQT::DownloadDirectory()
{
    if (!ui.sync_toggle->isChecked() && !sync_thread->isRunning())
    {
        Client* client = Client::GetClientInstance();
        client->DownloadServerDir();
    }
    else
        QMessageBox::warning(this, "Directory Download", "Stop the sync before downloading!");
}
void Client_OneDriveQT::DownloadFile()
{
    if (ui.sync_toggle->isChecked() || sync_thread->isRunning())
    {
        QMessageBox::warning(this, "File Download", "Stop the sync before downloading!");
        return;
    }

    try
    {
        Client* client = Client::GetClientInstance();
        std::string file_path = file_model->filePath(ui.FileTreeView->currentIndex()).toStdString();

        client->DownloadServerFile(file_path);
    }
    catch (const std::exception& except)
    {
        //TODO: log exception
    }
}
void Client_OneDriveQT::UploadFile()
{
    if (ui.sync_toggle->isChecked() || sync_thread->isRunning())
    {
        QMessageBox::warning(this, "File Upload", "Stop the sync before uploading!");
        return;
    }

    try
    {
        Client* client = Client::GetClientInstance();
        std::string file_path = file_model->filePath(ui.FileTreeView->currentIndex()).toStdString();

        client->UploadFileToServer(file_path);
    }
    catch (const std::exception& except)
    {
        //TODO: log exception
    }
}


void Client_OneDriveQT::ToggleSync()
{
    if (ui.sync_toggle->isChecked())
        StartSync();
    else
        StopSync();
}
void Client_OneDriveQT::StartSync()
{
    ui.sync_toggle->setChecked(true);
    sync_timer->start(1000);
}
void Client_OneDriveQT::StopSync()
{
    ui.sync_toggle->setChecked(false);
    sync_thread->exit();
    sync_timer->stop();
}


void Client_OneDriveQT::Logout()
{
    StopSync();
    Client* client = Client::GetClientInstance();
    client->DisconnectAndCleanup();
    
    //deleting file explorer
    delete dir_model;
    delete dir_filter;
    delete file_model;

    ChangePage(PageIndices::WELCOME_PAGE);
}

void Client_OneDriveQT::closeEvent(QCloseEvent* event)
{
    Client* client = Client::GetClientInstance();
    client->DisconnectAndCleanup();

    sync_thread->terminate();
}
