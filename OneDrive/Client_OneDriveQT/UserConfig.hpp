#pragma once

#include <string>

class UserConfig
{
	std::string m_username;
	std::string m_dir_path;

	const static std::string config_file_name;

	// I/O
	void ReadConfigFile();
public:
	UserConfig();

	//setters & getters
	void SetUserName(const std::string& username);
	void SetDirPath(const std::string& dir_path);

	std::string GetUsername() { return m_username; }
	std::string GetDirPath() { return m_dir_path; }

	void WriteConfigFile();
};
