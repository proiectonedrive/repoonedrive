#include "UserConfig.hpp"
#include <filesystem>
#include <fstream>

const std::string UserConfig::config_file_name = "user.config";

UserConfig::UserConfig() : m_username{ "" }, m_dir_path{ "" }
{
	if (std::filesystem::exists(config_file_name) && std::filesystem::is_regular_file(config_file_name))
		ReadConfigFile();
}

void UserConfig::ReadConfigFile()
{
	std::ifstream fin(config_file_name);

	std::string line;
	while (std::getline(fin, line))
	{
		if (line == "#username")
			std::getline(fin, m_username);
		else if (line == "#dir_path")
		{
			std::getline(fin, m_dir_path);
			if (!std::filesystem::is_directory(m_dir_path))
				m_dir_path = "";
		}
	}

	fin.close();
}

void UserConfig::WriteConfigFile()
{
	std::ofstream fout(config_file_name);
	fout << "#username\n" << m_username << "\n#dir_path\n" << m_dir_path;
	fout.close();
}


void UserConfig::SetUserName(const std::string& username)
{
	m_username = username;
}
void UserConfig::SetDirPath(const std::string& dir_path)
{
	m_dir_path = dir_path;
}