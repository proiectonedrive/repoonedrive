#pragma once

#include "qthread.h"
#include "Client.hpp"

class SyncThread : public QThread
{
	void run() override
	{
		Client* client = Client::GetClientInstance();
		client->Sync();
	}
};
