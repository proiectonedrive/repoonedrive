#include "DirTreeFilter.h"

DirTreeFilter::DirTreeFilter() :m_root_dir("") {}
DirTreeFilter::DirTreeFilter(const QString& root_dir) : m_root_dir(root_dir)
{
	m_root_dir += '/';
	m_root_dir.remove(0, 3);
}

void DirTreeFilter::SetRootDir(const QString& new_root_dir)
{
	m_root_dir = new_root_dir + '/';
	m_root_dir.remove(0, 3);
}

bool DirTreeFilter::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
	QModelIndex path_index = sourceModel()->index(source_row, 0, source_parent);
	QString path;
	
	while (path_index.parent().isValid())
	{
		path = sourceModel()->data(path_index).toString() + "/" + path;
		path_index = path_index.parent();
	}

	if (path.startsWith(m_root_dir) || m_root_dir.startsWith(path))
		return true;
	return false;
}
