#include "Client.hpp"
#include <Device.h>
#define maximum_option_size 3

Client::Client() : client_dir_path{ "" }, username { "" }, is_device_new { false }
{
	logger_client.createFile(".\\LogFilesClient");
}
Client* Client::GetClientInstance()
{
	static Client single_instance;//only created at the first call of this function, it mentains the same value afterwards
	return &single_instance;
}
void Client::SetClientDir(const std::string& client_dir_path)
{
	this->client_dir_path = client_dir_path;
}


// Connection functions *****************************************
bool Client::ConnectToServer()
{
	// Input data
	WSADATA WSAData;

	// Created socket server
	SOCKADDR_IN addr;

	if (WSAStartup(MAKEWORD(2, 0), &WSAData))
	{
		std::cout << "WSAStartup error";
		logger_client.writingFunction(1, " WSAStartup error");
		return false;
	}

	// If invalid socket created,
	// return
	if ((server = socket(AF_INET,
		SOCK_STREAM, 0))
		== INVALID_SOCKET) {
		std::cout << "Socket creation failed with error: "
			<< WSAGetLastError() << std::endl;
		std::string s;
		s.append(" Socket creation failed with error: ");
		s.append(std::to_string(WSAGetLastError()));
		logger_client.writingFunction(1, s);
		return false;
	}

	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(5555);

	// trying to connect to the server
	if (connect(server, (SOCKADDR*)&addr, sizeof(addr)) == SOCKET_ERROR) 
	{
		std::cout << "Server connection failed with error: "
			<< WSAGetLastError() << std::endl;
		std::string s;
		s.append(" Server connection failed with error: ");
		s.append(std::to_string(WSAGetLastError()));
		logger_client.writingFunction(0, s);

		return false;
	}

	// If connection established
	std::cout << "Connected to server!"
		<< std::endl;
	logger_client.writingFunction(0, " Connected to server!");

	return true;
}

bool Client::LoginRegister(const std::string& username, const CommunicationCodes::LoginCodes& flag)
{
		switch (flag)
		{
		case CommunicationCodes::LoginCodes::LOGIN://login attempt
		{
			if (lr::ValidateUsername(username))
			{
				TryLogin(username);
			}
			else
			{
				DisconnectAndCleanup();
				logger_client.writingFunction(0, " This username is invalid. Check for typos and try again!");
				throw std::exception("[ERROR] This username is invalid. Check for typos and try again!");
			}
			break;
		}
		case CommunicationCodes::LoginCodes::REGISTER://register
		{
			if (lr::ValidateUsername(username))
			{
				TryRegister(username);
			}
			else
			{
				DisconnectAndCleanup();
				logger_client.writingFunction(0, " The username that you inserted is not valid! Please check the rules carefully and try to register again!");
				throw std::exception("[ERROR] The username that you inserted is not valid! Please check the rules carefully and try to register again!\n\n- Length between 5 and 30 characters\n- Can contain lowercase and uppercase letters\n- Can also contain numbers and only one special character ( \".\" or \"_\" )");
			}
			break;
		}
		}
}

void Client::Sync()
{
	try 
	{
		enum SyncCommandCodes
		{
			ADD_FILE,
			REMOVE_FILE,
			RENAME_FILE
		};

		std::unordered_map <std::string, std::string> database_map, current_map;
		std::stack<std::tuple<SyncCommandCodes, std::string, std::string>> command_stack;
		std::string db_path = "OneDriveDBClient.db";
		int id_user = DataBaseConnection::getUserId("OneDriveDBClient.db", username);
		DataBaseConnection::selectFile(db_path.c_str(), database_map, id_user);

		std::unordered_set<std::string> marked_files;
		current_map.clear();
		metadata::IterateDirectoryHash(client_dir_path, current_map);
		for (auto& file_pair : current_map)
		{
			auto iter = database_map.find(file_pair.first);
			if (iter != database_map.end() && iter->second == file_pair.second)
				marked_files.insert(file_pair.first);
			else if (iter != database_map.end() && std::filesystem::path(iter->second).parent_path() == std::filesystem::path(file_pair.second).parent_path() && iter->second != file_pair.second)
			{
				command_stack.push(std::make_tuple(SyncCommandCodes::RENAME_FILE, file_pair.first, file_pair.second));
				marked_files.insert(file_pair.first);
			}
			else
				command_stack.push(std::make_tuple(SyncCommandCodes::ADD_FILE, file_pair.first, file_pair.second));
		}
		for (auto& file_pair : database_map)
		{
			if (marked_files.find(file_pair.first) == marked_files.end())
				if (current_map.find(file_pair.first) == current_map.end() || (current_map.find(file_pair.first) != current_map.end() && current_map.find(file_pair.first)->second != file_pair.second))
					command_stack.push(std::make_tuple(SyncCommandCodes::REMOVE_FILE, file_pair.first, file_pair.second));
		}

		while (!command_stack.empty())
		{
			auto current_command = command_stack.top();
			command_stack.pop();

			SyncCommandCodes command_type = std::get<0>(current_command);
			std::string file_hash = std::get<1>(current_command);
			std::string relative_path = std::get<2>(current_command);

			switch (command_type)
			{
			case SyncCommandCodes::ADD_FILE:
			{
				SyncAddFile(file_hash, relative_path, db_path, database_map);
				break;
			}
			case SyncCommandCodes::REMOVE_FILE:
			{
				SyncRemoveFile(file_hash, relative_path, db_path, database_map);
				break;
			}

			case SyncCommandCodes::RENAME_FILE:
			{
				SyncRenameFile(file_hash, relative_path, db_path, database_map);
				break;
			}
			}
		}
	}
	catch (const std::exception& except) {

		std::cerr << "Failed to sync the directory: " << except.what() << "\n";
	}
}

void Client::DownloadServerDir()
{
	try
	{
		DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::DOWNLOAD_DIRECTORY);
		if (DataTransfer::RecvAnswersCode(server) == CommunicationCodes::AnswersCodes::POSITIVE)
			DataTransfer::RecvDir(server, client_dir_path);
	}
	catch (const std::exception& except)
	{
		logger_client.writingFunction(1, except.what());
	}
}

void Client::DownloadServerFile(std::string& file_path)
{
	//making the path relative to the sync directory
	file_path.erase(file_path.begin(), file_path.begin() + client_dir_path.length() + 1);

	DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::DOWNLOAD_FILE);

	DataTransfer::SendText(server, file_path); //sending the path of the requested file
	CommunicationCodes::AnswersCodes server_answer = DataTransfer::RecvAnswersCode(server);

	if (server_answer == CommunicationCodes::AnswersCodes::NEGATIVE)
	{
		logger_client.writingFunction(0, "[!]: Requested file does not exist on the server!");
		throw std::exception("Requested file does not exist on the server!");
	}

	DataTransfer::RecvFile(server, client_dir_path); //if the file was found on the server it is downloaded
}

void Client::UploadFileToServer(std::string& file_path)
{
	std::replace(file_path.begin(), file_path.end(), '/', '\\');
	//getting file hash
	std::string file_hash = std::to_string(metadata::fnv1a(metadata::fileToString(file_path)));
	//making the path relative to the sync directory
	file_path.erase(file_path.begin(), file_path.begin() + client_dir_path.length() + 1);


	//DELETING OLD VERSION OF THE FILE FROM CLIENT DB
	int id_user = DataBaseConnection::getUserId("OneDriveDBClient.db", username);
	//specifying what we are about to send
	std::string old_file_hash = DataBaseConnection::getFileHash("OneDriveDBClient.db", file_path, id_user);
	DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::DELETE_FILE);

	DataTransfer::SendText(server, old_file_hash);//sending file hash so that the server can find which file to remove from the database
	DataTransfer::SendText(server, file_path);//sending the path of the file to be deleted

	//recieving deletion confirmation
	CommunicationCodes::AnswersCodes server_answer = DataTransfer::RecvAnswersCode(server);
	if (server_answer == CommunicationCodes::AnswersCodes::POSITIVE)
		std::cout << "File successfully deleted from server!\n";
	else
		std::cout << "[!]: File not fount on server\n";

	std::string sql_delete_command = "DELETE FROM file WHERE \"hash\" = ? AND id_user = ?;";
	sqlite3_stmt* stmt_delete_file{};
	sqlite3* db;
	if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
	{
		sqlite3_prepare_v2(db, sql_delete_command.c_str(), -1, &stmt_delete_file, NULL);
		sqlite3_bind_text(stmt_delete_file, 1, old_file_hash.c_str(), old_file_hash.length(), SQLITE_TRANSIENT);
		sqlite3_bind_int(stmt_delete_file, 2, id_user);
		sqlite3_step(stmt_delete_file);
	}
	else
	{
		std::cout << "Failed to open db\n";
	}
	sqlite3_finalize(stmt_delete_file);


	//UPLOADING NEW VERSION OF THE FILE
	//specifying what we are about to send
	DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::ADD_FILE);

	//necessary data for server database
	DataTransfer::SendText(server, file_hash);
	DataTransfer::SendText(server, file_path);

	//sending the actual file
	DataTransfer::SendFile(server, file_path, client_dir_path);

	//only update local database after we are sure that the server recieved the changes
	sqlite3_stmt* stmt_insert_file{};
	std::string sql_insert_command = "INSERT INTO \"file\" (id_user, \"path\", hash) VALUES(?,?,?);";

	if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
	{
		sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt_insert_file, NULL);
		sqlite3_bind_int(stmt_insert_file, 1, id_user);
		sqlite3_bind_text(stmt_insert_file, 2, file_path.c_str(), file_path.length(), SQLITE_TRANSIENT);
		sqlite3_bind_text(stmt_insert_file, 3, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
		sqlite3_step(stmt_insert_file);
	}
	else
	{
		std::cout << "Failed to open db\n";
	}

	sqlite3_finalize(stmt_insert_file);
	sqlite3_close(db);
}

void Client::DisconnectAndCleanup()
{
	try
	{
		// Socket closed
		DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::DISCONNECT);
	}
	catch (const std::exception& except){}

	closesocket(server);
	WSACleanup();
}

//*****************************************

// LoginRegister component functions *********************************

void Client::TryLogin(const std::string& username)
{
	CommunicationCodes::AnswersCodes server_answer;
	try
	{
		DataTransfer::SendLoginCode(server, CommunicationCodes::LoginCodes::LOGIN);
		DataTransfer::SendText(server, username);//send the username
		server_answer = DataTransfer::RecvAnswersCode(server);
	}
	catch (const std::exception& except)
	{
		DisconnectAndCleanup();
		logger_client.writingFunction(1, " Failed to send username");
		throw std::exception("[ERROR] Failed to send username.");
	}

	if (server_answer == CommunicationCodes::AnswersCodes::POSITIVE)//username found, connection successful
	{
		server_answer = DataTransfer::RecvAnswersCode(server);//recieving answer if username is already connected
		if (server_answer == CommunicationCodes::AnswersCodes::NEGATIVE)
		{
			logger_client.writingFunction(0, "[!]: This username is already connected!");
			throw std::exception("[!]: This username is already connected, please disconnect from other devices before proceeding!");
		}

		std::string device_name;
		device_name = Device::getDeviceName();
		DataTransfer::SendText(server, device_name);
		this->username = username;

		server_answer = DataTransfer::RecvAnswersCode(server);//checking if the device is new
		if (server_answer == CommunicationCodes::AnswersCodes::POSITIVE)
			is_device_new = true;

		if(!DataBaseConnection::findUser("OneDriveDBClient.db",username))
		{
			sqlite3_stmt* stmt_insert_user{};
			sqlite3* db;

			std::string sql_insert_command = "INSERT INTO \"user\" (username) VALUES(?);";

			if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
			{
				sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt_insert_user, NULL);
				sqlite3_bind_text(stmt_insert_user, 1, username.c_str(), username.length(), SQLITE_TRANSIENT);
				sqlite3_step(stmt_insert_user);
			}
			else
			{
				std::cout << "Failed to open db\n";
			}

			sqlite3_finalize(stmt_insert_user);
			sqlite3_close(db);
		}
	}
	else
	{
		DisconnectAndCleanup();
		logger_client.writingFunction(0, " This username does not exist. Check for typos or register an account!");
		throw std::exception("[ERROR] This username does not exist. Check for typos or register an account!");
	}
}

void Client::TryRegister(const std::string& username)
{
	CommunicationCodes::AnswersCodes server_answer;
	try
	{
		DataTransfer::SendLoginCode(server, CommunicationCodes::LoginCodes::REGISTER);
		DataTransfer::SendText(server, username);//send the username
		server_answer = DataTransfer::RecvAnswersCode(server);
	}
	catch (const std::exception& except)
	{
		DisconnectAndCleanup();
		logger_client.writingFunction(1, " Failed to send username");
		throw std::exception("[ERROR] Failed to send username.");
	}

	if (server_answer == CommunicationCodes::AnswersCodes::POSITIVE)
	{
		std::string device_name;
		device_name = Device::getDeviceName();
		DataTransfer::SendText(server, device_name);
		this->username = username;

		sqlite3_stmt* stmt_insert_user{};
		sqlite3* db;

		//int id_user = DataBaseConnection::getUserId("OneDriveDBClient.db", client_username);
		std::string sql_insert_command = "INSERT INTO \"user\" (username) VALUES(?);";

		if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt_insert_user, NULL);
			sqlite3_bind_text(stmt_insert_user, 1, username.c_str(), username.length(), SQLITE_TRANSIENT);
			sqlite3_step(stmt_insert_user);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_insert_user);
		sqlite3_close(db);
	}
	else
	{
		DisconnectAndCleanup();
		logger_client.writingFunction(0, " An account with this username already exists, try to login or create another account!");
		throw std::exception("[ERROR] An account with this username already exists, try to login or create another account!");
	}
}


// Sync component functions *****************************************
bool Client::SyncAddFile(const std::string& file_hash, const std::string& relative_path, 
	const std::string& db_path, std::unordered_map <std::string, std::string>& database_map) const
{
	try
	{
		//specifying what we are about to send
		DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::ADD_FILE);

		//necessary data for server database
		DataTransfer::SendText(server, file_hash);
		DataTransfer::SendText(server, relative_path);

		//sending the actual file
		DataTransfer::SendFile(server, relative_path, client_dir_path);

		//only update local database after we are sure that the server recieved the changes
		sqlite3_stmt* stmt_insert_file{};
		sqlite3* db;

		int id_user = DataBaseConnection::getUserId("OneDriveDBClient.db", username);
		std::string sql_insert_command = "INSERT INTO \"file\" (id_user, \"path\", hash) VALUES(?,?,?);";

		if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt_insert_file, NULL);
			sqlite3_bind_int(stmt_insert_file, 1, id_user);
			sqlite3_bind_text(stmt_insert_file, 2, relative_path.c_str(), relative_path.length(), SQLITE_TRANSIENT);
			sqlite3_bind_text(stmt_insert_file, 3, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
			sqlite3_step(stmt_insert_file);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_insert_file);
		sqlite3_close(db);

		database_map[file_hash] = relative_path;
	}
	catch (const std::exception& except)
	{
		std::cerr << "Failed to sync file on 'addFile' command --> " << except.what() << "\n";
		return false;
	}
	return true;
}

bool Client::SyncRemoveFile(const std::string& file_hash, const std::string& relative_path,
	const std::string& db_path, std::unordered_map <std::string, std::string>& database_map) const
{
	try
	{
		//specifying what we are about to send
		DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::DELETE_FILE);

		DataTransfer::SendText(server, file_hash);//sending file hash so that the server can find which file to remove from the database
		DataTransfer::SendText(server, relative_path);//sending the path of the file to be deleted

		//recieving deletion confirmation
		CommunicationCodes::AnswersCodes server_answer = DataTransfer::RecvAnswersCode(server);
		if (server_answer == CommunicationCodes::AnswersCodes::POSITIVE)
			std::cout << "File successfully deleted from server!\n";
		else
			std::cout << "[!]: File not fount on server\n";

		//only update local database after we are sure that the server recieved the changes
		sqlite3_stmt* stmt_delete_file{};
		sqlite3* db;

		int id_user = DataBaseConnection::getUserId("OneDriveDBClient.db", username);
		std::string sql_delete_command = "DELETE FROM file WHERE \"hash\" = ? AND id_user = ?;";

		if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_delete_command.c_str(), -1, &stmt_delete_file, NULL);
			sqlite3_bind_text(stmt_delete_file, 1, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
			sqlite3_bind_int(stmt_delete_file, 2, id_user);
			sqlite3_step(stmt_delete_file);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_delete_file);
		sqlite3_close(db);

		database_map.erase(file_hash);
	}
	catch (const std::exception& except)
	{
		std::cerr << "Failed to sync on 'removeFile' command --> " << except.what() << "\n";
		return false;
	}
	return true;
}

bool Client::SyncRenameFile(const std::string& file_hash, const std::string& relative_path,
	const std::string& db_path, std::unordered_map <std::string, std::string>& database_map) const
{
	try
	{
		//specifying what we are about to send
		DataTransfer::SendConnectionHandlerCode(server, CommunicationCodes::ConnectionHandlerCodes::RENAME_FILE);

		//necessary data for server database
		DataTransfer::SendText(server, file_hash);
		DataTransfer::SendText(server, relative_path);
		DataTransfer::SendText(server, database_map[file_hash]);

		//recieving renaming confirmation
		CommunicationCodes::AnswersCodes server_answer = DataTransfer::RecvAnswersCode(server);
		if (server_answer == CommunicationCodes::AnswersCodes::POSITIVE)
			std::cout << "File successfully renamed on server!\n";
		else
			std::cout << "[!]: File not fount on server\n";

		//only update local database after we are sure that the server recieved the changes
		sqlite3_stmt* stmt_update_file{};
		sqlite3* db;

		int id_user = DataBaseConnection::getUserId("OneDriveDBClient.db", username);
		std::string sql_update_command = "UPDATE file SET \"path\"= ? WHERE \"hash\" = ? AND id_user = ?;";

		if (sqlite3_open("OneDriveDBClient.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_update_command.c_str(), -1, &stmt_update_file, NULL);
			sqlite3_bind_text(stmt_update_file, 1, relative_path.c_str(), relative_path.length(), SQLITE_TRANSIENT);
			sqlite3_bind_text(stmt_update_file, 2, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
			sqlite3_bind_int(stmt_update_file, 3, id_user);
			sqlite3_step(stmt_update_file);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_update_file);
		sqlite3_close(db);
		database_map[file_hash] = relative_path;
	}
	catch (const std::exception& except)
	{
		std::cerr << "Failed to sync file on 'addFile' command --> " << except.what() << "\n";
		return false;
	}
	return true;
}

//*****************************************
