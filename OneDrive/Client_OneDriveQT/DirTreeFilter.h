#pragma once

#include <qsortfilterproxymodel.h>

class DirTreeFilter : public QSortFilterProxyModel
{
	QString m_root_dir;
public:
	//constructor that gets the client's directory
	DirTreeFilter();
	DirTreeFilter(const QString& root_dir);

	void SetRootDir(const QString& new_root_dir);

	//determines if a row will be included in the final model
	bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
};
