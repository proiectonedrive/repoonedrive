#define _WINSOCK_DEPRECATED_NO_WARNING
#include <iostream>
#include <fstream>
#include <string>
#include <charconv>
#include <cmath>
#include <winsock2.h>
#include <filesystem>
#include <QWidget>
#include <QMessageBox>
#include <stdlib.h>
#include <regex>
#include <unordered_map>
#include <unordered_set>
#include <stack>
#include "DataTransfer.h"
#include "ServerClientCommunicationCodes.h"
#include "LoginRegister.hpp"
#include "DataBaseConnection.hpp"
#include "MetadataExtraction.hpp"
#include "Logger.h"

#pragma comment(lib, "ws2_32.lib")


#pragma warning(disable:4996)
class Client//Singleton class
{
	Logger logger_client;
	std::string client_dir_path, username;
	SOCKET server;
	bool is_device_new;

	//private constructor, singleton class
	Client();
	Client(const Client& other_client) = delete;//singleton cannot be copied or assigned
	void operator=(const Client& other_client) = delete;

public:

	static Client* GetClientInstance();

	//setters & getters
	void SetClientDir(const std::string& client_dir_path);
	std::string GetClientDir() { return client_dir_path; }
	bool IsDeviceNew() { return is_device_new; }

	// Connection functions *****************************************

	// Driver Code
	bool ConnectToServer();

	//function that handles the login and register process
	bool LoginRegister(const std::string& username, const CommunicationCodes::LoginCodes& flag);

	//function that syncs directory, repeats until connection is lost
	void Sync();

	//downloads the directory from the server
	void DownloadServerDir();

	//downloads the version of a file stored on the server
	void DownloadServerFile(std::string& file_path);

	//upload single file to server
	void UploadFileToServer(std::string& file_path);

	//function that closes the connection
	void DisconnectAndCleanup();


private:
	//LoginRegister component functions
	void TryLogin(const std::string& username);

	void TryRegister(const std::string& username);


	//sync component functions
	bool SyncAddFile(const std::string& file_hash, const std::string& relative_path,
		const std::string& db_path, std::unordered_map <std::string, std::string>& database_map) const;

	bool SyncRemoveFile(const std::string& file_hash, const std::string& relative_path,
		const std::string& db_path, std::unordered_map <std::string, std::string>& database_map) const;

	bool SyncRenameFile(const std::string& file_hash, const std::string& relative_path,
		const std::string& db_path, std::unordered_map <std::string, std::string>& database_map) const;

	//*****************************************
};