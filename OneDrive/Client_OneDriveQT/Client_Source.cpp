#include "Client_OneDriveQT.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    //instantiating the client class
    Client* client = Client::GetClientInstance();

    QApplication application(argc, argv);
    Client_OneDriveQT window;
    window.show();
    return application.exec();
}
