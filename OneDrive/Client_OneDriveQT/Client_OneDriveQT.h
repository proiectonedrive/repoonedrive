#pragma once

#include <QtWidgets/QMainWindow>
#include <qfilesystemmodel.h>
#include <qtimer.h>
#include "ui_Client_OneDriveQT.h"
#include "ServerClientCommunicationCodes.h"
#include "DirTreeFilter.h"
#include "UserConfig.hpp"
#include "thread"
#include "qmenu.h"
#include "FileHandler.h"
#include "SyncThread.h"

enum class PageIndices
{
    WELCOME_PAGE,
    LOGIN_PAGE,
    REGISTER_PAGE,
    MAIN_PAGE,
    SETTINGS_PAGE
};

class Client_OneDriveQT : public QMainWindow
{
    Q_OBJECT

public:
    Client_OneDriveQT(QWidget* parent = Q_NULLPTR);
    void SetupWelcomePage();
    void SetupLoginPage();
    void SetupRegisterPage();
    void SetupMainPage();
    void SetupSettingsPage();

private slots:
    void LoginRegisterClicked(const CommunicationCodes::LoginCodes& flag);
    void ThreadFinished();

private:
    UserConfig user_config;

    //dir view
    DirTreeFilter* dir_filter;
    QFileSystemModel* dir_model;

    //file view
    QFileSystemModel* file_model;
    QMenu* right_click_file_menu;
    QAction* delete_file;
    QAction* download_file;
    QAction* upload_file;

    //polling
    SyncThread* sync_thread;
    QTimer* sync_timer;

    Ui::Client_OneDriveQTClass ui;
    void InitFileExplorer(const QString& client_dir_path);
    void InitDirView(const QString& client_dir_path);
    void InitFileView(const QString& client_dir_path);

    void ChangePage(const PageIndices& page_index);
    void ChangeFileView(const QModelIndex& view_index);


    void ChangeClientDir();
    void DeleteFile();
    void DownloadDirectory();
    void DownloadFile();
    void UploadFile();

    void ToggleSync();
    void StartSync();
    void StopSync();

    void Logout();

    void closeEvent(QCloseEvent* event) override;
};
