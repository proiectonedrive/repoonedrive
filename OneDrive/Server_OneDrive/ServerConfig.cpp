#include "ServerConfig.hpp"
#include <fstream>
#include <filesystem>

const std::string ServerConfig::config_file_name = "server.config";

ServerConfig::ServerConfig() : m_storage_dir_path{ "" }
{
	if (std::filesystem::exists(config_file_name) && std::filesystem::is_regular_file(config_file_name))
		ReadConfigFile();
}

void ServerConfig::ReadConfigFile()
{
	std::ifstream fin(config_file_name);

	std::string line;
	while (std::getline(fin, line))
	{
		if (line == "#dir_path")
		{
			std::getline(fin, m_storage_dir_path);
			if (!std::filesystem::is_directory(m_storage_dir_path))
				m_storage_dir_path = "";
		}
	}

	fin.close();
}

void ServerConfig::WriteConfigFile()
{
	std::ofstream fout(config_file_name);
	fout <<"#dir_path\n" << m_storage_dir_path;
	fout.close();
}

void ServerConfig::SetDirPath(const std::string& dir_path)
{
	m_storage_dir_path = dir_path;
}