#include "ConnectedUsers.hpp"

void ConnectedUsers::AddUser(const std::string& new_user)
{
	mtx.lock();
	m_connected_users.insert(new_user);
	mtx.unlock();
}

void ConnectedUsers::DeleteUser(const std::string& user)
{
	mtx.lock();
	m_connected_users.erase(user);
	mtx.unlock();
}

bool ConnectedUsers::FindUser(const std::string& user)
{
	mtx.lock();
	if (m_connected_users.find(user) != m_connected_users.end())
	{
		mtx.unlock();
		return true;
	}
	mtx.unlock();
	return false;
}