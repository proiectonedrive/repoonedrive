#include <iostream>
#include <fstream>
#include <string>
#include <charconv>
#include <winsock2.h>
#include <filesystem>
#include <thread>
#include "FileHandler.h"
#include "DataTransfer.h"
#include "ServerClientCommunicationCodes.h"
#include "DataBaseConnection.hpp"
#include "MetadataExtraction.hpp"
#include "ServerConfig.hpp"
#pragma comment (lib, "ws2_32.lib")

namespace ServerConnection {

	// Connection functions *****************************************

	//function that handles the login and register process
	bool LoginRegisterHandler(const SOCKET& client, const std::string& client_username);
	bool Login(const SOCKET& client, std::string& client_username);
	bool Register(const SOCKET& client, std::string& client_username);

	//function that handles connection
	void ConnectionHandler(const SOCKET client);
	void RecieveText(const SOCKET client, const std::string& client_username);
	void RecieveFile(const SOCKET client, const std::string& client_username, const std::string user_dir);
	void RecieveDirectory(const SOCKET client, const std::string& user_dir);
	void RecieveDeleteFileCommand(const SOCKET client, const std::string& client_username, const std::string user_dir);
	void RecieveRenameFileCommand(const SOCKET client, const std::string& client_username, const std::string user_dir);
	void SendFile(const SOCKET client, const std::string user_dir);
	void SendDirectory(const SOCKET client, const std::string user_dir);
	void CloseConnectionAndCleanup(const SOCKET client, const std::string& client_username, const std::string& disconnect_message);

	//setup functions
	void SetupServerConfig();

	// Driver Code
	void Connection();

	//*****************************************
}