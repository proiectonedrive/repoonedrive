#include <unordered_set>
#include <mutex>

class ConnectedUsers
{
	std::unordered_set<std::string> m_connected_users;
	std::mutex mtx;

public:
	ConnectedUsers() {};
	void AddUser(const std::string& new_user);
	void DeleteUser(const std::string& user);
	bool FindUser(const std::string& user);
};