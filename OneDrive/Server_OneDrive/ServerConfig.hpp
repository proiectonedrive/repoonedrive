#pragma once

#include <string>

class ServerConfig
{
	std::string m_storage_dir_path;

	const static std::string config_file_name;

	// I/O
	void ReadConfigFile();
public:
	ServerConfig();

	//setters & getters
	void SetDirPath(const std::string& dir_path);

	std::string GetDirPath() { return m_storage_dir_path; }

	void WriteConfigFile();
};
