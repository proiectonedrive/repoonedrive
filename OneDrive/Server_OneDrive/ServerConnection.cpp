#include "ServerConnection.hpp"
#include "Logger.h"
#include "ConnectedUsers.hpp"

#define maximum_option_size 3
// Connection functions *****************************************

namespace ServerConnection
{
	Logger loggerServer;
	std::string storage_dir_path;
	ConnectedUsers connected_users;

	bool LoginRegisterHandler(const SOCKET& client, std::string& client_username)
	{
		CommunicationCodes::LoginCodes client_option_code;
		
		while (true)
		{
			//recieve login/register code
			try
			{
				client_option_code = DataTransfer::RecvLoginCode(client);
			}
			catch (const std::exception& except)
			{
				std::cerr << "Lost connection with socket " << client << " --> " << except.what() << "\n";
				
				std::string s;
				s.append(" Lost connection with socket ");
				s.append(std::to_string(client));
				s.append(" : ");
				s.append(except.what());

				loggerServer.writingFunction(1, s);
				return false;
			}

			switch (client_option_code)
			{
			case CommunicationCodes::LoginCodes::DISCONNECT: //disconnect
			{
				std::cout << "Lost connection with socket " << client << ".\n";

				loggerServer.writingFunction(1, " Lost connection with socket " + client);
				return false;
			}
			case CommunicationCodes::LoginCodes::LOGIN: //login attempt
			{
				try
				{
					if (Login(client, client_username))
						return true;
				}
				catch (const std::exception& except)
				{
					std::cerr << "Failed to login user: " << except.what() << "\n";
					std::string s;
					s.append(" Failed to login user : ");
					s.append(except.what());
					loggerServer.writingFunction(1, s);
					return false;
				}
				break;
			}
			case CommunicationCodes::LoginCodes::REGISTER: //register new user
			{
				try
				{
					if (Register(client, client_username))
						return true;
				}
				catch (const std::exception& except)
				{
					std::cerr << "Failed to register user: " << except.what() << "\n";
					std::string s;
					s.append(" Failed to register user : ");
					s.append(except.what());
					loggerServer.writingFunction(1, s);
					return false;
				}
				break;
			}
			}
		}
	}
	bool Login(const SOCKET& client, std::string& client_username)
	{
		client_username = DataTransfer::RecvText(client);
		if (DataBaseConnection::findUser("OneDriveDBServer.db", client_username))
		{
			//telling the client that the username is valid
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);

			//checking if this username is already connected
			if (connected_users.FindUser(client_username))
			{
				//if found, do not allow the client to connect with this username
				DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
				return false;
			}
			//if not found, add to the user set
			connected_users.AddUser(client_username);
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);

			std::string device_name;
			device_name = DataTransfer::RecvText(client);
			if (!DataBaseConnection::findDevice("OneDriveDBServer.db", client_username, device_name))
			{
				//telling the client that the device is new
				DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);
				sqlite3_stmt* stmt{};
				sqlite3* db;

				int id_user = DataBaseConnection::getUserId("OneDriveDBServer.db", client_username);
				std::string sql_insert_command = "INSERT INTO \"device\" (device_name,id_user) VALUES(?,?);";

				if (sqlite3_open("OneDriveDBServer.db", &db) == SQLITE_OK)
				{
					sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt, NULL);
					sqlite3_bind_text(stmt, 1, device_name.c_str(), device_name.length(), SQLITE_TRANSIENT);
					sqlite3_bind_int(stmt, 2, id_user);
					sqlite3_step(stmt);
				}
				else
				{
					std::cout << "Failed to open db\n";
					loggerServer.writingFunction(0, "Failed to open db");
				}

				sqlite3_finalize(stmt);
				sqlite3_close(db);
			}
			else //telling the client that the device is not new
				DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);

			std::string s;
			s.append(" Client on socket ");
			s.append(std::to_string(client));
			s.append(" logged in with username ");
			s.append(client_username);

			std::cout << "Client on socket " << client << " logged in with username " << client_username << "\n";
			loggerServer.writingFunction(0, s);
			return true;
		}
		else
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
		return false;
	}
	bool Register(const SOCKET& client, std::string& client_username)
	{
		client_username = DataTransfer::RecvText(client);
		if (!DataBaseConnection::findUser("OneDriveDBServer.db", client_username))
		{
			sqlite3_stmt* stmt_insert_user{};
			sqlite3* db;

			std::string sql_insert_command = "INSERT INTO \"user\" (username) VALUES(?);";

			if (sqlite3_open("OneDriveDBServer.db", &db) == SQLITE_OK)
			{
				sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt_insert_user, NULL);
				sqlite3_bind_text(stmt_insert_user, 1, client_username.c_str(), client_username.length(), SQLITE_TRANSIENT);
				sqlite3_step(stmt_insert_user);
				//////////////////////////////////////////
				fh::CreateDir(storage_dir_path, client_username);
				std::cout << "Client on socket " << client << " registered with username " << client_username << "\n\n";
				std::string s;
				s.append(" Client on socket ");
				s.append(std::to_string(client));
				s.append(" registered with username ");
				s.append(client_username);

				loggerServer.writingFunction(0, s);
				DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);

				//device
				std::string device_name;
				device_name = DataTransfer::RecvText(client);
				if (!DataBaseConnection::findDevice("OneDriveDBServer.db", client_username, device_name))
				{

					sqlite3_stmt* stmt_insert_device{};

					int id_user = DataBaseConnection::getUserId("OneDriveDBServer.db", client_username);
					std::string sql_insert_device = "INSERT INTO \"device\" (device_name,id_user) VALUES(?,?);";

					if (sqlite3_open("OneDriveDBServer.db", &db) == SQLITE_OK)
					{
						sqlite3_prepare_v2(db, sql_insert_device.c_str(), -1, &stmt_insert_device, NULL);
						sqlite3_bind_text(stmt_insert_device, 1, device_name.c_str(), device_name.length(), SQLITE_TRANSIENT);
						sqlite3_bind_int(stmt_insert_device, 2, id_user);
						sqlite3_step(stmt_insert_device);
					}
					else
					{
						std::cout << "Failed to open db\n";
					}

					sqlite3_finalize(stmt_insert_device);
				}
				sqlite3_finalize(stmt_insert_user);
				sqlite3_close(db);
				return true;
			}
			else
			{
				std::cout << "Unknown error at inserting in the database." << "\n\n";
				loggerServer.writingFunction(1, " Unknown error at inserting in the database.");
				DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
				sqlite3_finalize(stmt_insert_user);
				sqlite3_close(db);
			}
		}
		else
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
		return false;
	}


	void ConnectionHandler(const SOCKET client)
	{
		std::string client_username;
		if (!LoginRegisterHandler(client, client_username))
		{
			closesocket(client);
			return;
		}

		std::string user_dir = storage_dir_path + "\\" + client_username;
		CommunicationCodes::ConnectionHandlerCodes client_option_code; // fist, the server recieves the option that was selected 
																	  //by the client user
																	 // in order to know what operations to execute
		std::string db_path = "OneDriveDBServer.db";

		// Server executes continuously
		while (true) {

			try
			{
				client_option_code = DataTransfer::RecvConnectionHandlerCode(client);
			}
			catch (const std::exception& except)
			{
				CloseConnectionAndCleanup(client, client_username, except.what());
				return;
			}

			std::cout <<"[INFO] " << client_username << " sent option: " << static_cast<int>(client_option_code) << "\n";
			switch (client_option_code)
			{
			case CommunicationCodes::ConnectionHandlerCodes::DISCONNECT: //client disconnects
			{
				CloseConnectionAndCleanup(client, client_username, "Disconnect signal recieved");
				return;
			}
			case CommunicationCodes::ConnectionHandlerCodes::TEXT_MESSAGE: //client sends text
			{
				try 
				{
					RecieveText(client, client_username);
				}
				catch (const std::exception &except) 
				{
					std::string disconnect_message = "Failed to recieve message: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				}
				break;
			}
			case CommunicationCodes::ConnectionHandlerCodes::ADD_FILE:
			{
				try
				{
					RecieveFile(client, client_username, user_dir);
				}
				catch (std::exception& except)
				{
					std::string disconnect_message = "Failed to recieve file: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				}
				break;
			}
			case CommunicationCodes::ConnectionHandlerCodes::DIRECTORY:
			{
				try
				{
					RecieveDirectory(client, user_dir);
				}
				catch (const std::exception &except) 
				{
					std::string disconnect_message = "Failed to recieve directory: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				} 
				break;
			}
			case CommunicationCodes::ConnectionHandlerCodes::DELETE_FILE:
			{
				try
				{
					RecieveDeleteFileCommand(client, client_username, user_dir);
				}
				catch (std::exception& except)
				{
					std::string disconnect_message = "Failed to sync on file deletion: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				}
				break;
			}
			case CommunicationCodes::ConnectionHandlerCodes::RENAME_FILE:
			{
				try
				{
					RecieveRenameFileCommand(client, client_username, user_dir);
				}
				catch (std::exception& except)
				{
					std::string disconnect_message = "Failed to rename file: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				}
				break;
			}
			case CommunicationCodes::ConnectionHandlerCodes::DOWNLOAD_FILE:
			{
				try
				{
					SendFile(client, user_dir);
				}
				catch (const std::exception &except) 
				{
					std::string disconnect_message = "Failed to send requested file: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				}
				break;
			}
			case CommunicationCodes::ConnectionHandlerCodes::DOWNLOAD_DIRECTORY:
			{
				try
				{				
					SendDirectory(client, user_dir);
				}
				catch (const std::exception &except) {
					std::string disconnect_message = "Failed to send requested directory: ";
					disconnect_message.append(except.what());
					CloseConnectionAndCleanup(client, client_username, disconnect_message);
					return;
				}
				break;
			}
			default:
				break;
			}
		}
	}
	void RecieveText(const SOCKET client, const std::string& client_username)
	{
		std::string message = DataTransfer::RecvText(client);
		std::cout << "Client " << client_username << " said: " << message << "\n";
		loggerServer.writingFunction(0, " Client " + client_username + " said: " + message);
	}
	void RecieveFile(const SOCKET client, const std::string& client_username, const std::string user_dir)
	{
		std::string file_hash, relative_path;
		file_hash = DataTransfer::RecvText(client); //recieving the file hash: to be added into the database
		relative_path = DataTransfer::RecvText(client); //recieving the relative file path: to be added into the database

		DataTransfer::RecvFile(client, user_dir);//recieving the file

		//updating server database to contain new file

		sqlite3_stmt* stmt_insert_file{};
		sqlite3* db;

		int id_user = DataBaseConnection::getUserId("OneDriveDBServer.db", client_username);
		std::string sql_insert_command = "INSERT INTO \"file\" (\"path\", id_user, hash) VALUES(?,?,?);";

		if (sqlite3_open("OneDriveDBServer.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_insert_command.c_str(), -1, &stmt_insert_file, NULL);
			sqlite3_bind_text(stmt_insert_file, 1, relative_path.c_str(), relative_path.length(), SQLITE_TRANSIENT);
			sqlite3_bind_int(stmt_insert_file, 2, id_user);
			sqlite3_bind_text(stmt_insert_file, 3, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
			sqlite3_step(stmt_insert_file);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_insert_file);
		sqlite3_close(db);

		std::cout << "[!]: File recieved from " << client_username << ": " << relative_path << "\n";
		loggerServer.writingFunction(1, "[!]: File recieved from " + client_username + ": " + relative_path);
	}
	void RecieveDirectory(const SOCKET client, const std::string& user_dir)
	{
		DataTransfer::RecvDir(client, user_dir);
	}
	void RecieveDeleteFileCommand(const SOCKET client, const std::string& client_username, const std::string user_dir)
	{
		std::string file_hash, relative_path;

		file_hash = DataTransfer::RecvText(client);
		relative_path = DataTransfer::RecvText(client);
		std::filesystem::path file_path = user_dir + "\\" + relative_path;

		if (!std::filesystem::exists(file_path))//we notify the client that the requested deletion order cannot be completed
		{
			std::cout << "[!]: Deletion order did not find file: " << file_path.string() << "\n";
			loggerServer.writingFunction(1, " Deletion order did not find file: " + file_path.string());
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
			return;
		}

		if (std::filesystem::is_directory(file_path))
			fh::DeleteDir(file_path.string());
		else
		{
			fh::FileRemove(file_path.string());
			std::filesystem::path aux_file_path = file_path;
			aux_file_path = aux_file_path.parent_path();
			while (is_empty(aux_file_path) && aux_file_path != user_dir)
			{
				fh::DeleteDir(aux_file_path.string());
				aux_file_path = aux_file_path.parent_path();
			}
		}

		std::cout << "[OK]: Deleted file: " << file_path.string() << "\n";
		loggerServer.writingFunction(1, " Deleted file: " + file_path.string());
		DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);

		sqlite3_stmt* stmt_delete_file{};
		sqlite3* db;

		int id_user = DataBaseConnection::getUserId("OneDriveDBServer.db", client_username);
		std::string sql_delete_command = "DELETE FROM file WHERE \"hash\" = ? AND id_user = ?;";

		if (sqlite3_open("OneDriveDBServer.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_delete_command.c_str(), -1, &stmt_delete_file, NULL);
			sqlite3_bind_text(stmt_delete_file, 1, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
			sqlite3_bind_int(stmt_delete_file, 2, id_user);
			sqlite3_step(stmt_delete_file);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_delete_file);
		sqlite3_close(db);
	}
	void RecieveRenameFileCommand(const SOCKET client, const std::string& client_username, const std::string user_dir)
	{
		std::string file_hash, relative_path, old_path;
		file_hash = DataTransfer::RecvText(client); //recieving the file hash: to be added into the database
		relative_path = DataTransfer::RecvText(client); //recieving the relative file path: to be added into the database
		old_path = DataTransfer::RecvText(client); //recieving the old file path the file had before: to form the current server path of the file
		std::filesystem::path old_file_path = user_dir + "\\" + old_path;
		std::filesystem::path new_file_path = user_dir + "\\" + relative_path;

		if (!std::filesystem::exists(old_file_path))//we notify the client that the requested renaming order cannot be completed
		{
			std::cout << "[!]: Renaming order did not find file: " << old_file_path.string() << "\n";
			loggerServer.writingFunction(1, " Renaming order did not find file: " + old_file_path.string());
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
			return;
		}

		// Renaming the file
		fh::FileRename(old_file_path.string(), new_file_path.string());

		std::cout << "[OK]: Renamed file: " << old_file_path.string() << "to: " << new_file_path.string() << "\n";
		loggerServer.writingFunction(1, " Renamed file: " + old_file_path.string() + "to: " + new_file_path.string());
		DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);

		//updating server database to contain new path of file
		sqlite3_stmt* stmt_update_file{};
		sqlite3* db;

		int id_user = DataBaseConnection::getUserId("OneDriveDBServer.db", client_username);
		std::string sql_update_command = "UPDATE file SET \"path\"= ? WHERE \"hash\"= ? AND id_user = ?;";

		if (sqlite3_open("OneDriveDBServer.db", &db) == SQLITE_OK)
		{
			sqlite3_prepare_v2(db, sql_update_command.c_str(), -1, &stmt_update_file, NULL);
			sqlite3_bind_text(stmt_update_file, 1, relative_path.c_str(), relative_path.length(), SQLITE_TRANSIENT);
			sqlite3_bind_text(stmt_update_file, 2, file_hash.c_str(), file_hash.length(), SQLITE_TRANSIENT);
			sqlite3_bind_int(stmt_update_file, 3, id_user);
			sqlite3_step(stmt_update_file);
		}
		else
		{
			std::cout << "Failed to open db\n";
		}

		sqlite3_finalize(stmt_update_file);
		sqlite3_close(db);
	}
	void SendFile(const SOCKET client, const std::string user_dir)
	{
		std::string user_input;
		user_input = DataTransfer::RecvText(client);
		std::filesystem::path file_path = user_dir + "\\" + user_input;

		if (!std::filesystem::exists(file_path) || std::filesystem::is_directory(file_path))
		{
			//inform the client that the file does not exist
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
			std::cerr << "[!]: Requested file does not exist:\n path: " << file_path.string() << "\n\n";
			loggerServer.writingFunction(0, " Requested file does not exist: path: " + file_path.string());
			return;
		}

		//the file exists, informing the client that it will be sent
		DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);
		DataTransfer::SendFile(client, user_input, user_dir);
	}
	void SendDirectory(const SOCKET client, const std::string user_dir)
	{
		if (!std::filesystem::exists(user_dir) || !std::filesystem::is_directory(user_dir))
		{
			//inform the client that the directory does not exist
			DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::NEGATIVE);
			std::cerr << "[!]: Requested directory does not exist:\n path: " << user_dir << "\n\n";
			loggerServer.writingFunction(0, " Requested directory does not exist :  path: " + user_dir);
			return;
		}

		//the directory exists, informing the client that it will be sent
		DataTransfer::SendAnswersCode(client, CommunicationCodes::AnswersCodes::POSITIVE);
		DataTransfer::SendDir(client, user_dir);
	}
	void CloseConnectionAndCleanup(const SOCKET client, const std::string& client_username, const std::string& disconnect_message)
	{
		closesocket(client);
		connected_users.DeleteUser(client_username);

		std::cout << "INFO:  " << client_username << " disconnected --> " << disconnect_message << "\n";
		loggerServer.writingFunction(0, client_username + " disconnected --> " + disconnect_message);
	}


	void SetupServerConfig()
	{
		//creating logger file
		loggerServer.createFile("LogFilesServer");

		//checking for stored config file
		ServerConfig server_config;
		storage_dir_path = server_config.GetDirPath();

		//ask for input storage dir, necessary for recieved files
		while (!std::filesystem::exists(storage_dir_path) || !std::filesystem::is_directory(storage_dir_path))
		{
			std::cout
				<< "Please insert storage directory path (clients will be unable to connect before a valid path is given):\n>";
			getline(std::cin, storage_dir_path);

		}

		//if the final path is different from the one in the configuration, then update config file
		if (storage_dir_path != server_config.GetDirPath())
		{
			server_config.SetDirPath(storage_dir_path);
			server_config.WriteConfigFile();
		}

		std::cout << "Server storage directory is: " << storage_dir_path << "\n";
	}

	void Connection()
	{
		SetupServerConfig();

		// Data
		WSADATA WSAData;

		// Created socket server
		// and client
		SOCKET server, client;

		// Socket address for server
		// and client
		SOCKADDR_IN serverAddr, clientAddr;

		if (WSAStartup(MAKEWORD(2, 0), &WSAData))
		{
			std::cout << "WSAStartup error";
			loggerServer.writingFunction(1, " WSAStartup error");
			return;
		}

		// Making server
		server = socket(AF_INET,
			SOCK_STREAM, 0);

		// If invalid socket created,
		// return -1
		if (server == INVALID_SOCKET) {
			std::cout << " Socket creation failed with error:"
				<< WSAGetLastError() << std::endl;
			std::string s;
			s.append(" Socket creation failed with error: ");
			s.append(std::to_string(WSAGetLastError()));
			loggerServer.writingFunction(1, s);
			return;
		}
		serverAddr.sin_addr.s_addr = INADDR_ANY;
		serverAddr.sin_family = AF_INET;
		serverAddr.sin_port = htons(5555);

		// If socket error occurred,
		// return -1
		if (bind(server,
			(SOCKADDR*)&serverAddr,
			sizeof(serverAddr))
			== SOCKET_ERROR) {
			std::cout << "Bind function failed with error: "
				<< WSAGetLastError() << std::endl;
			std::string s;
			s.append(" Bind function failed with error: ");
			s.append(std::to_string(WSAGetLastError()));
			loggerServer.writingFunction(1, s);

			return;
		}

		// Get the request from
		// server
		if (listen(server, 0)
			== SOCKET_ERROR) {
			std::cout << "Listen function failed with error:"
				<< WSAGetLastError() << std::endl;
			std::string s;
			s.append(" Listen function failed with error: ");
			s.append(std::to_string(WSAGetLastError()));
			loggerServer.writingFunction(0, s);
			return;
		}

		std::cout << "Listening for incoming connections...." << std::endl;

		// Initialize client address
		int clientAddrSize = sizeof(clientAddr);

		// If connection established
		while (true) {
			client = accept(server, (SOCKADDR*)&clientAddr, &clientAddrSize);
			std::cout << "Client connected on socket: " << client << std::endl;
			loggerServer.writingFunction(1, " Client connected on socket: " + client);
			// Create Thread t1
			std::thread t1(ServerConnection::ConnectionHandler, client);
			t1.detach();
		}

		// Close the socket
		closesocket(client);

		// If socket closing
		// failed.
		if (closesocket(server)
			== SOCKET_ERROR) {
			std::cout << "Close socket failed with error: "
				<< WSAGetLastError() << std::endl;
			std::string s;
			s.append(" Close socket failed with error: ");
			s.append(std::to_string(WSAGetLastError()));
			loggerServer.writingFunction(1, s);
			return;
		}
		WSACleanup();
	}

	//*****************************************
}